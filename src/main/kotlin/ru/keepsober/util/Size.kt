package ru.keepsober.util

data class Size(
    val width: Double,
    val height: Double
) {
    constructor(width: Number, height: Number) : this(width.toDouble(), height.toDouble())
}
