package ru.keepsober.util

import kotlinx.html.dom.create
import kotlinx.html.js.link
import org.w3c.dom.HTMLLinkElement
import org.w3c.dom.get
import kotlin.browser.document

fun loadCss(url: String) {
    val link: HTMLLinkElement = document.create.link {
        type = "text/css"
        rel = "stylesheet"
        href = url
    }
    document.getElementsByTagName("head")[0]?.appendChild(link);
}
