package ru.keepsober

import kotlinx.html.InputType
import kotlinx.html.dom.create
import kotlinx.html.id
import kotlinx.html.js.a
import kotlinx.html.js.br
import kotlinx.html.js.button
import kotlinx.html.js.img
import kotlinx.html.js.input
import kotlinx.html.js.onChangeFunction
import kotlinx.html.js.onClickFunction
import kotlinx.html.js.option
import kotlinx.html.js.select
import kotlinx.html.role
import kotlinx.html.style
import kotlinx.html.tabIndex
import org.w3c.dom.HTMLImageElement
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.HTMLOptionElement
import org.w3c.dom.HTMLSelectElement
import org.w3c.dom.Node
import org.w3c.dom.events.Event
import org.w3c.dom.get
import org.w3c.files.FileReader
import org.w3c.files.get
import ru.keepsober.wrapper.fabric.Canvas
import ru.keepsober.wrapper.fontfaceobserver.FontFaceObserver
import kotlin.browser.document

fun buildCanvas() = Canvas("canvas").apply { renderOnAddRemove = true }

fun buildImageElement(onLoadFun: () -> Unit) = document.create.img {
    style = "display: none"
}.apply { onload = { onLoadFun() } }

fun buildInputImage(imageElement: HTMLImageElement) = document.create.input {
    style = "display: none"
    type = InputType.file
    accept = "image/*"
    onChangeFunction = { event ->
        val file = (event.target as? HTMLInputElement)?.files?.get(0) ?: throw Exception("can't get file")
        val fileReader = FileReader()
        fileReader.onloadend = {
            imageElement.src = fileReader.result as String
            Unit
        }
        fileReader.readAsDataURL(file)
    }
}

fun buildBtnLoadImage(inputImage: HTMLInputElement) = document.create.button {
    id = "chooseImage"
    +"Выбрать фото"
    onClickFunction = {
        inputImage.click()
    }
}.apply {
    classList.add("btn", "btn-primary", "btn-lg")
}

fun buildBtnRotateImage(onClickFun: (Event) -> Unit) = document.create.button {
    id = "rotateImage"
    +"Повернуть"
    onClickFunction = onClickFun
}.apply {
    classList.add("btn", "btn-primary", "btn-lg")
}

fun buildBtnSaveImage(saveFun: (Event) -> Unit) = document.create.a {
    id = "saveImage"
    +"Сохранить"
    role = "button"
    tabIndex="5"
    onClickFunction = saveFun
}.apply {
    classList.add("btn", "btn-primary", "btn-lg")
}

fun createBlankA() = document.create.a()

fun buildBr() = document.create.br()

fun buildLookSelect(onChange: (String) -> Unit) = document.create.select {
    id = "lookSelect"
    onChangeFunction = {
        val htmlSelectElement = it.target as HTMLSelectElement
        val selectedValue = (htmlSelectElement.options[htmlSelectElement.selectedIndex] as HTMLOptionElement).value
        onChange(selectedValue)
    }
}.apply {
    Config.looks.forEachIndexed { index, look ->
        document.create.option {
            +look.color
            value = index.toString()
        }.let(::appendChild)
    }
}

fun buildFontFamilySelect(onChange: (String) -> Unit) = document.create.select {
    id = "fontFamily"
    onChangeFunction = {
        val htmlSelectElement = it.target as HTMLSelectElement
        val selectedValue = (htmlSelectElement.options[htmlSelectElement.selectedIndex] as HTMLOptionElement).value
        onChange(selectedValue)
    }
}.apply {
    Config.fontLooks.forEachIndexed { index, fontLook ->

        val addFont: (font: String) -> Node = {
            document.create.option {
                +it
                value = index.toString()
            }.let(::appendChild)
        }

        if (fontLook.system) {
            addFont(fontLook.font)
        } else {
            FontFaceObserver(fontLook.font).load().then {
                addFont(fontLook.font)
            }.catch {
                println("catch font face observer $fontLook")
            }
        }
    }
}
