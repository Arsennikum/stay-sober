@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")

package ru.keepsober.wrapper.fontfaceobserver

import kotlin.js.Promise

@JsModule("fontfaceobserver")
open external class FontFaceObserver(fontFamilyName: String, variant: FontVariant? = definedExternally /* null */) {
    open fun load(testString: String? = definedExternally /* null */, timeout: Number? = definedExternally /* null */): Promise<Unit>
}
