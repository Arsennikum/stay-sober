@file:JsModule("fontfaceobserver")
@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION")
package ru.keepsober.wrapper.fontfaceobserver

external interface FontVariant {
    var weight: dynamic /* Number | String */
        get() = definedExternally
        set(value) = definedExternally
    var style: String?
        get() = definedExternally
        set(value) = definedExternally
    var stretch: String?
        get() = definedExternally
        set(value) = definedExternally
}
