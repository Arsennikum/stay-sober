package ru.keepsober.wrapper.fabric

typealias IUtilAminEaseFunction = (t: Number, b: Number, c: Number, d: Number) -> Number
