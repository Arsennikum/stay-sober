@file:Suppress(
    "INTERFACE_WITH_SUPERCLASS",
    "OVERRIDING_FINAL_MEMBER",
    "RETURN_TYPE_MISMATCH_ON_OVERRIDE",
    "CONFLICTING_OVERLOADS",
    "EXTERNAL_DELEGATION",
    "NESTED_CLASS_IN_EXTERNAL_INTERFACE"
)
@file:JsModule("fabric")
@file:JsQualifier("fabric")
package ru.keepsober.wrapper.fabric

import org.w3c.dom.CanvasGradient
import org.w3c.dom.CanvasPattern
import org.w3c.dom.CanvasRenderingContext2D
import org.w3c.dom.HTMLCanvasElement
import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLImageElement
import org.w3c.dom.HTMLTextAreaElement
import org.w3c.dom.events.Event
import org.w3c.dom.events.MouseEvent
import org.w3c.dom.svg.SVGElement
import org.w3c.dom.svg.SVGGradientElement
import org.w3c.xhr.XMLHttpRequest
import kotlin.js.Json
import kotlin.js.RegExp

external var isLikelyNode: Boolean = definedExternally
external var isTouchSupported: Boolean = definedExternally
external var version: String = definedExternally
external var textureSize: Number = definedExternally
external fun createCanvasForNode(width: Number, height: Number): Canvas = definedExternally
external fun createSVGRefElementsMarkup(canvas: StaticCanvas): String = definedExternally
external fun createSVGFontFacesMarkup(objects: Array<Any>): String = definedExternally
external fun loadSVGFromString(
    string: String,
    callback: (results: Array<Any>, options: Any) -> Unit,
    reviver: Function<*>? = definedExternally /* null */
): Unit = definedExternally

external fun loadSVGFromURL(
    url: String,
    callback: (results: Array<Any>, options: Any) -> Unit,
    reviver: Function<*>? = definedExternally /* null */
): Unit = definedExternally

external fun getCSSRules(doc: SVGElement): Any = definedExternally
external fun parseElements(
    elements: Array<Any>,
    callback: Function<*>,
    options: Any,
    reviver: Function<*>? = definedExternally /* null */
): Unit = definedExternally

external fun parsePointsAttribute(points: String): Array<Any> = definedExternally
external fun parseStyleAttribute(element: SVGElement): Any = definedExternally
external fun parseElements(
    elements: Array<SVGElement>,
    callback: Function<*>,
    options: Any? = definedExternally /* null */,
    reviver: Function<*>? = definedExternally /* null */
): Unit = definedExternally

external interface `T$0` {
    @nativeGetter
    operator fun get(key: String): String?

    @nativeSetter
    operator fun set(key: String, value: String)
}

external fun parseAttributes(
    element: HTMLElement,
    attributes: Array<String>,
    svgUid: String? = definedExternally /* null */
): `T$0` = definedExternally

external fun getGradientDefs(doc: SVGElement): Json = definedExternally
external fun parseFontDeclaration(value: String, oStyle: Any): Unit = definedExternally
external fun parseSVGDocument(
    doc: SVGElement,
    callback: (results: Array<Any>, options: Any) -> Unit,
    reviver: Function<*>? = definedExternally /* null */
): Unit = definedExternally

external fun parseTransformAttribute(attributeValue: String): Array<Number> = definedExternally
external fun log(vararg values: Any): Unit = definedExternally
external fun warn(vararg values: Any): Unit = definedExternally
external interface IDataURLOptions {
    var format: String? get() = definedExternally; set(value) = definedExternally
    var quality: Number? get() = definedExternally; set(value) = definedExternally
    var multiplier: Number? get() = definedExternally; set(value) = definedExternally
    var left: Number? get() = definedExternally; set(value) = definedExternally
    var top: Number? get() = definedExternally; set(value) = definedExternally
    var width: Number? get() = definedExternally; set(value) = definedExternally
    var height: Number? get() = definedExternally; set(value) = definedExternally
    var enableRetinaScaling: Boolean? get() = definedExternally; set(value) = definedExternally
    var withoutTransform: Boolean? get() = definedExternally; set(value) = definedExternally
    var withoutShadow: Boolean? get() = definedExternally; set(value) = definedExternally
}

external interface `T$1` {
    var corner: String
    var original: Any
    var originX: String
    var originY: String
    var width: Number
}

external interface IEvent {
    var e: Event
    var target: Any? get() = definedExternally; set(value) = definedExternally
    var subTargets: Array<Any>? get() = definedExternally; set(value) = definedExternally
    var button: Number? get() = definedExternally; set(value) = definedExternally
    var isClick: Boolean? get() = definedExternally; set(value) = definedExternally
    var pointer: Point? get() = definedExternally; set(value) = definedExternally
    var absolutePointer: Point? get() = definedExternally; set(value) = definedExternally
    var transform: `T$1`? get() = definedExternally; set(value) = definedExternally
}

external interface IFillOptions {
    var source: dynamic /* String | HTMLImageElement */
    var repeat: String? get() = definedExternally; set(value) = definedExternally
    var offsetX: Number? get() = definedExternally; set(value) = definedExternally
    var offsetY: Number? get() = definedExternally; set(value) = definedExternally
}

external interface IToSVGOptions {
    var suppressPreamble: Boolean
    var viewBox: IViewBox
    var encoding: String
    var width: Number
    var height: Number
}

external interface IViewBox {
    var x: Number
    var y: Number
    var width: Number
    var height: Number
}

external interface ICollection<T> {
    fun add(vararg `object`: Any): T
    fun insertAt(`object`: Any, index: Number, nonSplicing: Boolean): T
    fun remove(vararg `object`: Any): T
    fun forEachObject(
        callback: (element: Any, index: Number, array: Array<Any>) -> Unit,
        context: Any? = definedExternally /* null */
    ): T

    fun getObjects(type: String? = definedExternally /* null */): Array<Any>
    fun item(index: Number): T
    fun isEmpty(): Boolean
    fun size(): Number
    fun contains(`object`: Any): Boolean
    fun complexity(): Number
}

external interface `T$2` {
    @nativeGetter
    operator fun get(eventName: String): ((e: IEvent) -> Unit)?

    @nativeSetter
    operator fun set(eventName: String, value: (e: IEvent) -> Unit)
}

external interface IObservable<T> {
    fun on(eventName: String, handler: (e: IEvent) -> Unit): T
    fun on(events: `T$2`): T
    fun trigger(eventName: String, options: Any? = definedExternally /* null */): T
    fun off(
        eventName: String? = definedExternally /* null */,
        handler: ((e: IEvent) -> Unit)? = definedExternally /* null */
    ): T

    fun off(
        eventName: Any? = definedExternally /* null */,
        handler: ((e: IEvent) -> Unit)? = definedExternally /* null */
    ): T

    fun off(): T
}

external interface Callbacks {
    var onComplete: Function<*>? get() = definedExternally; set(value) = definedExternally
    var onChange: Function<*>? get() = definedExternally; set(value) = definedExternally
}

external interface ICanvasAnimation<T> {
    var FX_DURATION: Number
    fun fxCenterObjectH(`object`: Any, callbacks: Callbacks? = definedExternally /* null */): T
    fun fxCenterObjectV(`object`: Any, callbacks: Callbacks? = definedExternally /* null */): T
    fun fxRemove(`object`: Any): T
    fun fxStraightenObject(`object`: Any): T
}

external interface IObjectAnimation<T> {
    fun animate(property: String, value: Number, options: IAnimationOptions? = definedExternally /* null */): Any
    fun animate(property: String, value: String, options: IAnimationOptions? = definedExternally /* null */): Any
    fun animate(properties: Any, options: IAnimationOptions? = definedExternally /* null */): Any
}

external interface IAnimationOptions {
    var from: dynamic /* String | Number */ get() = definedExternally; set(value) = definedExternally
    var duration: Number? get() = definedExternally; set(value) = definedExternally
    var onChange: Function<*>? get() = definedExternally; set(value) = definedExternally
    var onComplete: Function<*>? get() = definedExternally; set(value) = definedExternally
    var easing: Function<*>? get() = definedExternally; set(value) = definedExternally
    var by: Number? get() = definedExternally; set(value) = definedExternally
}

open external class Color(color: String? = definedExternally /* null */) {
    open fun getSource(): Array<Number> = definedExternally
    open fun setSource(source: Array<Number>): Unit = definedExternally
    open fun toRgb(): String = definedExternally
    open fun toRgba(): String = definedExternally
    open fun toHsl(): String = definedExternally
    open fun toHsla(): String = definedExternally
    open fun toHex(): String = definedExternally
    open fun toHexa(): String = definedExternally
    open fun getAlpha(): Number = definedExternally
    open fun setAlpha(alpha: Number): Unit = definedExternally
    open fun toGrayscale(): Color = definedExternally
    open fun toBlackWhite(threshold: Number): Color = definedExternally
    open fun overlayWith(otherColor: String): Color = definedExternally
    open fun overlayWith(otherColor: Color): Color = definedExternally

    companion object {
        fun fromRgb(color: String): Color = definedExternally
        fun fromRgba(color: String): Color = definedExternally
        fun sourceFromRgb(color: String): Array<Number> = definedExternally
        fun fromHsl(color: String): Color = definedExternally
        fun fromHsla(color: String): Color = definedExternally
        fun sourceFromHsl(color: String): Array<Number> = definedExternally
        fun fromHex(color: String): Color = definedExternally
        fun sourceFromHex(color: String): Array<Number> = definedExternally
        fun fromSource(source: Array<Number>): Color = definedExternally
    }
}

external interface `T$3` {
    var x1: Number? get() = definedExternally; set(value) = definedExternally
    var y1: Number? get() = definedExternally; set(value) = definedExternally
    var x2: Number? get() = definedExternally; set(value) = definedExternally
    var y2: Number? get() = definedExternally; set(value) = definedExternally
    var r1: Number? get() = definedExternally; set(value) = definedExternally
    var r2: Number? get() = definedExternally; set(value) = definedExternally
}

external interface IGradientOptions {
    var offsetX: Number? get() = definedExternally; set(value) = definedExternally
    var offsetY: Number? get() = definedExternally; set(value) = definedExternally
    var type: String? get() = definedExternally; set(value) = definedExternally
    var coords: `T$3`? get() = definedExternally; set(value) = definedExternally
    var colorStops: Any? get() = definedExternally; set(value) = definedExternally
    var gradientTransform: Any? get() = definedExternally; set(value) = definedExternally
}

open external class Gradient : IGradientOptions {
    open fun addColorStop(colorStop: Any): Gradient = definedExternally
    open fun toObject(propertiesToInclude: Any? = definedExternally /* null */): Any = definedExternally
    open fun toSVG(`object`: Any): String = definedExternally
    open fun toLive(ctx: CanvasRenderingContext2D): CanvasGradient = definedExternally

    companion object {
        fun fromElement(el: SVGGradientElement, instance: Any): Gradient = definedExternally
        fun forObject(obj: Any, options: IGradientOptions? = definedExternally /* null */): Gradient = definedExternally
    }
}

open external class Intersection(status: String? = definedExternally /* null */) {
    open fun appendPoint(point: Point): Intersection = definedExternally
    open fun appendPoints(points: Array<Point>): Intersection = definedExternally

    companion object {
        fun intersectLineLine(a1: Point, a2: Point, b1: Point, b2: Point): Intersection = definedExternally
        fun intersectLinePolygon(a1: Point, a2: Point, points: Array<Point>): Intersection = definedExternally
        fun intersectPolygonPolygon(points1: Array<Point>, points2: Array<Point>): Intersection = definedExternally
        fun intersectPolygonRectangle(points: Array<Point>, r1: Number, r2: Number): Intersection = definedExternally
    }
}

external interface IPatternOptions {
    var repeat: String? get() = definedExternally; set(value) = definedExternally
    var offsetX: Number? get() = definedExternally; set(value) = definedExternally
    var offsetY: Number? get() = definedExternally; set(value) = definedExternally
    var crossOrigin: dynamic /* '' | 'anonymous' | 'use-credentials' */ get() = definedExternally; set(value) = definedExternally
    var patternTransform: Array<Number>? get() = definedExternally; set(value) = definedExternally
    var source: dynamic /* String | HTMLImageElement */
}

open external class Pattern(options: IPatternOptions? = definedExternally /* null */) : IPatternOptions {
    override var source: dynamic = definedExternally
    open var id: Number = definedExternally
    open fun toObject(propertiesToInclude: Any): Any = definedExternally
    open fun toSVG(`object`: Any): String = definedExternally
    open fun setOptions(options: IPatternOptions): Unit = definedExternally
    open fun toLive(ctx: CanvasRenderingContext2D): CanvasPattern = definedExternally
}

open external class Point(x: Number, y: Number) {
    open var x: Number = definedExternally
    open var y: Number = definedExternally
    open var type: String = definedExternally
    open fun add(that: Point): Point = definedExternally
    open fun addEquals(that: Point): Point = definedExternally
    open fun scalarAdd(scalar: Number): Point = definedExternally
    open fun scalarAddEquals(scalar: Number): Point = definedExternally
    open fun subtract(that: Point): Point = definedExternally
    open fun subtractEquals(that: Point): Point = definedExternally
    open fun scalarSubtract(scalar: Number): Point = definedExternally
    open fun scalarSubtractEquals(scalar: Number): Point = definedExternally
    open fun multiply(scalar: Number): Point = definedExternally
    open fun multiplyEquals(scalar: Number): Point = definedExternally
    open fun divide(scalar: Number): Point = definedExternally
    open fun divideEquals(scalar: Number): Point = definedExternally
    open fun eq(that: Point): Point = definedExternally
    open fun lt(that: Point): Point = definedExternally
    open fun lte(that: Point): Point = definedExternally
    open fun gt(that: Point): Point = definedExternally
    open fun gte(that: Point): Point = definedExternally
    open fun lerp(that: Point, t: Number): Point = definedExternally
    open fun distanceFrom(that: Point): Number = definedExternally
    open fun midPointFrom(that: Point): Point = definedExternally
    open fun min(that: Point): Point = definedExternally
    open fun max(that: Point): Point = definedExternally
    override fun toString(): String = definedExternally
    open fun setXY(x: Number, y: Number): Point = definedExternally
    open fun setX(x: Number): Point = definedExternally
    open fun setY(y: Number): Point = definedExternally
    open fun setFromPoint(that: Point): Point = definedExternally
    open fun swap(that: Point): Point = definedExternally
    open fun clone(): Point = definedExternally
}

external interface IShadowOptions {
    var color: String? get() = definedExternally; set(value) = definedExternally
    var blur: Number? get() = definedExternally; set(value) = definedExternally
    var offsetX: Number? get() = definedExternally; set(value) = definedExternally
    var offsetY: Number? get() = definedExternally; set(value) = definedExternally
    var affectStroke: Boolean? get() = definedExternally; set(value) = definedExternally
    var includeDefaultValues: Boolean? get() = definedExternally; set(value) = definedExternally
    var nonScaling: Boolean? get() = definedExternally; set(value) = definedExternally
}

open external class Shadow : IShadowOptions {
    constructor(options: IShadowOptions?)
    constructor(options: String?)

    open fun initialize(options: IShadowOptions? = definedExternally /* null */): Shadow = definedExternally
    open fun initialize(options: String? = definedExternally /* null */): Shadow = definedExternally
    override fun toString(): String = definedExternally
    open fun toSVG(`object`: Any): String = definedExternally
    open fun toObject(): Any = definedExternally
    open fun initialize(): Shadow = definedExternally

    companion object {
        var reOffsetsAndBlur: RegExp = definedExternally
    }
}

external interface ICanvasDimensions {
    var width: Number
    var height: Number
}

external interface ICanvasDimensionsOptions {
    var backstoreOnly: Boolean? get() = definedExternally; set(value) = definedExternally
    var cssOnly: Boolean? get() = definedExternally; set(value) = definedExternally
}

external interface `T$4` {
    var x: Number
    var y: Number
}

external interface `T$5` {
    var tl: `T$4`
    var tr: `T$4`
    var bl: `T$4`
    var br: `T$4`
}

external interface IStaticCanvasOptions {
    var backgroundColor: dynamic /* String | Pattern */ get() = definedExternally; set(value) = definedExternally
    var backgroundImage: dynamic /* Image | String */ get() = definedExternally; set(value) = definedExternally
    var overlayColor: dynamic /* String | Pattern */ get() = definedExternally; set(value) = definedExternally
    var overlayImage: Image? get() = definedExternally; set(value) = definedExternally
    var includeDefaultValues: Boolean? get() = definedExternally; set(value) = definedExternally
    var stateful: Boolean? get() = definedExternally; set(value) = definedExternally
    var renderOnAddRemove: Boolean? get() = definedExternally; set(value) = definedExternally
    val clipTo: ((context: CanvasRenderingContext2D) -> Unit)? get() = definedExternally
    var controlsAboveOverlay: Boolean? get() = definedExternally; set(value) = definedExternally
    var allowTouchScrolling: Boolean? get() = definedExternally; set(value) = definedExternally
    var imageSmoothingEnabled: Boolean? get() = definedExternally; set(value) = definedExternally
    var viewportTransform: Array<Number>? get() = definedExternally; set(value) = definedExternally
    var backgroundVpt: Boolean? get() = definedExternally; set(value) = definedExternally
    var overlayVpt: Boolean? get() = definedExternally; set(value) = definedExternally
    var enableRetinaScaling: Boolean? get() = definedExternally; set(value) = definedExternally
    var vptCoords: `T$5`? get() = definedExternally; set(value) = definedExternally
    var skipOffscreen: Boolean? get() = definedExternally; set(value) = definedExternally
    var clipPath: Any? get() = definedExternally; set(value) = definedExternally
    var svgViewportTransformation: Boolean? get() = definedExternally; set(value) = definedExternally
}

external interface FreeDrawingBrush {
    var color: String
    var width: Number
}

open external class StaticCanvas :
    IStaticCanvasOptions/*, IObservable<StaticCanvas>, ICollection<StaticCanvas>, ICanvasAnimation<StaticCanvas>*/ {
    constructor(element: HTMLCanvasElement, options: ICanvasOptions?)
    constructor(element: String, options: ICanvasOptions?)

    open var _activeObject: dynamic /* Any | Group */ = definedExternally
    open var freeDrawingBrush: FreeDrawingBrush = definedExternally
    open fun calcOffset(): Canvas = definedExternally
    open fun setOverlayImage(
        image: Image,
        callback: Function<*>,
        options: IImageOptions? = definedExternally /* null */
    ): Canvas = definedExternally

    open fun setOverlayImage(
        image: String,
        callback: Function<*>,
        options: IImageOptions? = definedExternally /* null */
    ): Canvas = definedExternally

    open fun setBackgroundImage(
        image: Image,
        callback: Function<*>,
        options: IImageOptions? = definedExternally /* null */
    ): Canvas = definedExternally

    open fun setBackgroundImage(
        image: String,
        callback: Function<*>,
        options: IImageOptions? = definedExternally /* null */
    ): Canvas = definedExternally

    open fun setOverlayColor(overlayColor: String, callback: Function<*>): Canvas = definedExternally
    open fun setOverlayColor(overlayColor: Pattern, callback: Function<*>): Canvas = definedExternally
    open fun setBackgroundColor(backgroundColor: String, callback: Function<*>): Canvas = definedExternally
    open fun setBackgroundColor(backgroundColor: Pattern, callback: Function<*>): Canvas = definedExternally
    open fun getWidth(): Number = definedExternally
    open fun getHeight(): Number = definedExternally
    open fun setWidth(value: Number, options: ICanvasDimensionsOptions? = definedExternally /* null */): Canvas =
        definedExternally

    open fun setWidth(value: String, options: ICanvasDimensionsOptions? = definedExternally /* null */): Canvas =
        definedExternally

    open fun setHeight(value: Number, options: ICanvasDimensionsOptions? = definedExternally /* null */): Canvas =
        definedExternally

    open fun setHeight(value: String, options: ICanvasDimensionsOptions? = definedExternally /* null */): Canvas =
        definedExternally

    open fun setDimensions(
        dimensions: ICanvasDimensions,
        options: ICanvasDimensionsOptions? = definedExternally /* null */
    ): Canvas = definedExternally

    open fun getZoom(): Number = definedExternally
    open fun setViewportTransform(vpt: Array<Number>): Canvas = definedExternally
    open fun zoomToPoint(point: Point, value: Number): Canvas = definedExternally
    open fun setZoom(value: Number): Canvas = definedExternally
    open fun absolutePan(point: Point): Canvas = definedExternally
    open fun relativePan(point: Point): Canvas = definedExternally
    open fun getElement(): HTMLCanvasElement = definedExternally
    open fun clearContext(ctx: CanvasRenderingContext2D): Canvas = definedExternally
    open fun getContext(): CanvasRenderingContext2D = definedExternally
    open fun clear(): Canvas = definedExternally
    open fun renderAll(): Canvas = definedExternally
    open fun renderAndReset(): Canvas = definedExternally
    open fun calcViewportBoundaries(): `T$6` = definedExternally
    open fun renderCanvas(ctx: CanvasRenderingContext2D, objects: Array<Any>): Canvas = definedExternally
    open fun drawClipPathOnCanvas(ctx: CanvasRenderingContext2D): Unit = definedExternally
    open fun getCenter(): `T$7` = definedExternally
    open fun centerObjectH(`object`: Any): Canvas = definedExternally
    open fun centerObjectV(`object`: Any): Canvas = definedExternally
    open fun centerObject(`object`: Any): Canvas = definedExternally
    open fun viewportCenterObject(`object`: Any): Canvas = definedExternally
    open fun viewportCenterObjectH(`object`: Any): Canvas = definedExternally
    open fun viewportCenterObjectV(`object`: Any): Canvas = definedExternally
    open fun getVpCenter(): Point = definedExternally
    open fun toDatalessJSON(propertiesToInclude: Array<String>? = definedExternally /* null */): String =
        definedExternally

    open fun toObject(propertiesToInclude: Array<String>? = definedExternally /* null */): Any = definedExternally
    open fun toDatalessObject(propertiesToInclude: Array<String>? = definedExternally /* null */): Any =
        definedExternally

    open fun toSVG(
        options: IToSVGOptions? = definedExternally /* null */,
        reviver: Function<*>? = definedExternally /* null */
    ): String = definedExternally

    open fun sendToBack(`object`: Any): Canvas = definedExternally
    open fun bringToFront(`object`: Any): Canvas = definedExternally
    open fun sendBackwards(`object`: Any, intersecting: Boolean? = definedExternally /* null */): Canvas =
        definedExternally

    open fun bringForward(`object`: Any, intersecting: Boolean? = definedExternally /* null */): Canvas =
        definedExternally

    open fun moveTo(`object`: Any, index: Number): Canvas = definedExternally
    open fun dispose(): Canvas = definedExternally
    override fun toString(): String = definedExternally
    open fun toDataURL(options: IDataURLOptions? = definedExternally /* null */): String = definedExternally
    open fun clone(
        callback: Any? = definedExternally /* null */,
        properties: Array<String>? = definedExternally /* null */
    ): Unit = definedExternally
    open fun remove(o: Any): Unit = definedExternally

    open fun cloneWithoutData(callback: Any? = definedExternally /* null */): Unit = definedExternally
    open fun loadFromDatalessJSON(
        json: Any,
        callback: Function<*>,
        reviver: Function<*>? = definedExternally /* null */
    ): Canvas = definedExternally

    open fun loadFromJSON(
        json: Any,
        callback: Function<*>,
        reviver: Function<*>? = definedExternally /* null */
    ): Canvas = definedExternally

    open fun createSVGFontFacesMarkup(objects: Array<Any>): String = definedExternally
    open fun createSVGRefElementsMarkup(): String = definedExternally
    open fun straightenObject(`object`: Any): Canvas = definedExternally

    companion object {
        var EMPTY_JSON: String = definedExternally
        fun supports(methodName: String /* "getImageData" */): Boolean = definedExternally
        fun supports(methodName: String /* "toDataURL" */): Boolean = definedExternally
        fun supports(methodName: String /* "toDataURLWithQuality" */): Boolean = definedExternally
        fun supports(methodName: String /* "setLineDash" */): Boolean = definedExternally
        fun toJSON(propertiesToInclude: Array<String>? = definedExternally /* null */): String = definedExternally
    }
}

external interface `T$6` {
    var tl: Point
    var br: Point
    var tr: Point
    var bl: Point
}

external interface `T$7` {
    var top: Number
    var left: Number
}

external interface ICanvasOptions : IStaticCanvasOptions {
    var uniScaleTransform: Boolean? get() = definedExternally; set(value) = definedExternally
    var uniScaleKey: String? get() = definedExternally; set(value) = definedExternally
    var centeredScaling: Boolean? get() = definedExternally; set(value) = definedExternally
    var centeredRotation: Boolean? get() = definedExternally; set(value) = definedExternally
    var fill: dynamic /* String | Pattern */ get() = definedExternally; set(value) = definedExternally
    var centeredKey: String? get() = definedExternally; set(value) = definedExternally
    var altActionKey: String? get() = definedExternally; set(value) = definedExternally
    var interactive: Boolean? get() = definedExternally; set(value) = definedExternally
    var selection: Boolean? get() = definedExternally; set(value) = definedExternally
    var selectionKey: dynamic /* String | Array<String> */ get() = definedExternally; set(value) = definedExternally
    var altSelectionKey: String? get() = definedExternally; set(value) = definedExternally
    var selectionColor: String? get() = definedExternally; set(value) = definedExternally
    var selectionDashArray: Array<Number>? get() = definedExternally; set(value) = definedExternally
    var selectionBorderColor: String? get() = definedExternally; set(value) = definedExternally
    var selectionLineWidth: Number? get() = definedExternally; set(value) = definedExternally
    var selectionFullyContained: Boolean? get() = definedExternally; set(value) = definedExternally
    var hoverCursor: String? get() = definedExternally; set(value) = definedExternally
    var moveCursor: String? get() = definedExternally; set(value) = definedExternally
    var defaultCursor: String? get() = definedExternally; set(value) = definedExternally
    var freeDrawingCursor: String? get() = definedExternally; set(value) = definedExternally
    var rotationCursor: String? get() = definedExternally; set(value) = definedExternally
    var notAllowedCursor: String? get() = definedExternally; set(value) = definedExternally
    var containerClass: String? get() = definedExternally; set(value) = definedExternally
    var perPixelTargetFind: Boolean? get() = definedExternally; set(value) = definedExternally
    var targetFindTolerance: Number? get() = definedExternally; set(value) = definedExternally
    var skipTargetFind: Boolean? get() = definedExternally; set(value) = definedExternally
    var isDrawingMode: Boolean? get() = definedExternally; set(value) = definedExternally
    var preserveObjectStacking: Boolean? get() = definedExternally; set(value) = definedExternally
    var snapAngle: Number? get() = definedExternally; set(value) = definedExternally
    var snapThreshold: Number? get() = definedExternally; set(value) = definedExternally
    var stopContextMenu: Boolean? get() = definedExternally; set(value) = definedExternally
    var fireRightClick: Boolean? get() = definedExternally; set(value) = definedExternally
    var fireMiddleClick: Boolean? get() = definedExternally; set(value) = definedExternally
}

open external class Canvas : ICanvasOptions, StaticCanvas {
    constructor(element: HTMLCanvasElement, options: ICanvasOptions?)
    constructor(element: String, options: ICanvasOptions? = definedExternally)

    fun add(element: Object)

    open var _objects: Array<Any> = definedExternally
    override fun renderAll(): Canvas = definedExternally
    open fun renderTop(): Canvas = definedExternally
    open fun containsPoint(e: Event, target: Any, point: `T$4`? = definedExternally /* null */): Boolean =
        definedExternally

    open fun isTargetTransparent(target: Any, x: Number, y: Number): Boolean = definedExternally
    open fun setCursor(value: String): Unit = definedExternally
    open fun findTarget(e: MouseEvent, skipGroup: Boolean): Any = definedExternally
    open fun restorePointerVpt(pointer: Point): Any = definedExternally
    open fun getPointer(e: Event, ignoreZoom: Boolean? = definedExternally /* null */): `T$4` = definedExternally
    open fun getSelectionContext(): CanvasRenderingContext2D = definedExternally
    open fun getSelectionElement(): HTMLCanvasElement = definedExternally
    open fun getActiveObject(): dynamic = definedExternally
    open fun getActiveObjects(): Array<Any> = definedExternally
    open fun setActiveObject(`object`: Any, e: Event? = definedExternally /* null */): Canvas = definedExternally
    open fun discardActiveObject(e: Event? = definedExternally /* null */): Canvas = definedExternally
    override fun dispose(): Canvas = definedExternally
    override fun clear(): Canvas = definedExternally
    open fun drawControls(ctx: CanvasRenderingContext2D): Unit = definedExternally
    open fun removeListeners(): Unit = definedExternally

    companion object {
        var EMPTY_JSON: String = definedExternally
        fun supports(methodName: String /* "getImageData" */): Boolean = definedExternally
        fun supports(methodName: String /* "toDataURL" */): Boolean = definedExternally
        fun supports(methodName: String /* "toDataURLWithQuality" */): Boolean = definedExternally
        fun supports(methodName: String /* "setLineDash" */): Boolean = definedExternally
        fun toJSON(propertiesToInclude: Array<String>? = definedExternally /* null */): String = definedExternally
    }
}

external interface ICircleOptions : IObjectOptions {
    var radius: Number? get() = definedExternally; set(value) = definedExternally
    var startAngle: Number? get() = definedExternally; set(value) = definedExternally
    var endAngle: Number? get() = definedExternally; set(value) = definedExternally
}

open external class Circle(options: ICircleOptions? = definedExternally /* null */) : Object, ICircleOptions {
    open fun getRadiusX(): Number = definedExternally
    open fun getRadiusY(): Number = definedExternally
    open fun setRadius(value: Number): Number = definedExternally
    open fun _toSVG(): String = definedExternally

    companion object {
        var ATTRIBUTE_NAMES: Array<String> = definedExternally
        fun fromElement(element: SVGElement, options: ICircleOptions): Circle = definedExternally
        fun fromObject(`object`: Any): Circle = definedExternally
    }
}

external interface IEllipseOptions : IObjectOptions {
    var rx: Number? get() = definedExternally; set(value) = definedExternally
    var ry: Number? get() = definedExternally; set(value) = definedExternally
}

open external class Ellipse(options: IEllipseOptions? = definedExternally /* null */) /*: Object, IEllipseOptions*/ {
    open fun getRx(): Number = definedExternally
    open fun getRy(): Number = definedExternally
    open fun _toSVG(): String = definedExternally

    companion object {
        var ATTRIBUTE_NAMES: Array<String> = definedExternally
        fun fromElement(element: SVGElement, options: IEllipseOptions? = definedExternally /* null */): Ellipse =
            definedExternally

        fun fromObject(`object`: Any): Ellipse = definedExternally
    }
}

external interface IGroupOptions : IObjectOptions {
    var subTargetCheck: Boolean? get() = definedExternally; set(value) = definedExternally
    var useSetOnGroup: Boolean? get() = definedExternally; set(value) = definedExternally
}

open external class Group(
    objects: Array<Any>? = definedExternally /* null */,
    options: IGroupOptions? = definedExternally /* null */,
    isAlreadyGrouped: Boolean? = definedExternally /* null */
) : Object, ICollection<Group>, IGroupOptions {

    override fun add(vararg `object`: Any): Group
    override fun insertAt(`object`: Any, index: Number, nonSplicing: Boolean): Group = definedExternally
    override fun remove(vararg `object`: Any): Group = definedExternally
    override fun forEachObject (
        callback: (element: Any, index: Number, array: Array<Any>) -> Unit,
        context: Any?
    ): Group = kotlin.js.definedExternally
    override fun getObjects(type: String?): Array<Any> = definedExternally
    override fun item(index: Number): Group = definedExternally
    override fun isEmpty(): Boolean = definedExternally
    override fun size(): Number = definedExternally
    override fun contains(`object`: Any): Boolean = definedExternally

    open fun addWithUpdate(`object`: Any? = definedExternally /* null */): Group = definedExternally
    open fun removeWithUpdate(`object`: Any): Group = definedExternally
//    override fun render(ctx: CanvasRenderingContext2D): Unit = definedExternally
//    override fun shouldCache(): Boolean = definedExternally
//    override fun willDrawShadow(): Boolean = definedExternally
    open fun isOnACache(): Boolean = definedExternally
//    override fun drawObject(ctx: CanvasRenderingContext2D): Unit = definedExternally
//    override fun isCacheDirty(skipCanvas: Boolean? /* null */): Boolean = definedExternally
    open fun realizeTransform(`object`: Any): Any = definedExternally
    open fun destroy(): Group = definedExternally
    open fun toActiveSelection(): ActiveSelection = definedExternally
    open fun ungroupOnCanvas(): Group = definedExternally
    open fun setObjectsCoords(): Group = definedExternally
//    override fun toSVG(reviver: Function<*>?): String = definedExternally
//    override fun toClipPathSVG(reviver: Function<*>? /* null */): String = definedExternally
    open fun addWithUpdate(`object`: Any): Group = definedExternally

    companion object {
        fun fromObject(`object`: Any, callback: (group: Group) -> Any): Unit = definedExternally
    }
}

open external class ActiveSelection(
    objects: Array<Any>? = definedExternally /* null */,
    options: IObjectOptions? = definedExternally /* null */
): Group/*, ICollection<Group> уже есть в Group*/ {
    open fun toGroup(): Group = definedExternally

    companion object {
        fun fromObject(`object`: Any, callback: Function<*>): Unit = definedExternally
    }
}

external interface IImageOptions : IObjectOptions {
    var crossOrigin: String? get() = definedExternally; set(value) = definedExternally
    var srcFromAttribute: Boolean? get() = definedExternally; set(value) = definedExternally
    var minimumScaleTrigger: Number? get() = definedExternally; set(value) = definedExternally
    var cacheKey: String? get() = definedExternally; set(value) = definedExternally
    var cropX: Number? get() = definedExternally; set(value) = definedExternally
    var cropY: Number? get() = definedExternally; set(value) = definedExternally
    var filters: Array<IBaseFilter>? get() = definedExternally; set(value) = definedExternally
}

open external class Image : Object, IImageOptions {
    constructor(element: String?, options: IImageOptions?)
    constructor(element: HTMLImageElement?, options: IImageOptions?)

    open fun getElement(): HTMLImageElement = definedExternally
    open fun setElement(element: HTMLImageElement, options: IImageOptions? = definedExternally /* null */): Image =
        definedExternally

    open fun removeTexture(key: Any): Unit = definedExternally
    open fun dispose(): Unit = definedExternally
    open fun setCrossOrigin(value: String): Image = definedExternally
    open fun getOriginalSize(): `T$8` = definedExternally
    open fun hasCrop(): Boolean = definedExternally
    open fun _toSVG(): String = definedExternally
    open fun getSrc(): String = definedExternally
    open fun setSrc(
        src: String,
        callback: Function<*>? = definedExternally /* null */,
        options: IImageOptions? = definedExternally /* null */
    ): Image = definedExternally

    open fun applyResizeFilters(): Unit = definedExternally
    open fun applyFilters(filters: Array<IBaseFilter>? = definedExternally /* null */): Image = definedExternally
    open fun parsePreserveAspectRatioAttribute(): Any = definedExternally

    companion object {
        fun fromURL(
            url: String,
            callback: ((image: Image) -> Unit)? = definedExternally /* null */,
            imgOptions: IImageOptions? = definedExternally /* null */
        ): Image = definedExternally

        fun fromElement(
            element: SVGElement,
            callback: Function<*>,
            options: IImageOptions? = definedExternally /* null */
        ): Image = definedExternally

        var CSS_CANVAS: String = definedExternally
        var filters: IAllFilters = definedExternally
        var ATTRIBUTE_NAMES: Array<String> = definedExternally
    }
}

external interface `T$8` {
    var width: Number
    var height: Number
}

external interface ILineOptions : IObjectOptions {
    var x1: Number? get() = definedExternally; set(value) = definedExternally
    var x2: Number? get() = definedExternally; set(value) = definedExternally
    var y1: Number? get() = definedExternally; set(value) = definedExternally
    var y2: Number? get() = definedExternally; set(value) = definedExternally
}

open external class Line(
    points: Array<Number>? = definedExternally /* null */,
    objObjects: ILineOptions? = definedExternally /* null */
) : Object, ILineOptions {
    open fun _toSVG(): String = definedExternally
    open fun makeEdgeToOriginGetter(propertyNames: `T$9`, originValues: `T$10`): Function<*> = definedExternally
    open fun calcLinePoints(): `T$11` = definedExternally

    companion object {
        fun fromElement(
            element: SVGElement,
            callback: Function<*>? = definedExternally /* null */,
            options: ILineOptions? = definedExternally /* null */
        ): Line = definedExternally

        fun fromObject(`object`: Any): Line = definedExternally
        var ATTRIBUTE_NAMES: Array<String> = definedExternally
    }
}

external interface `T$9` {
    var origin: Number
    var axis1: Any
    var axis2: Any
    var dimension: Any
}

external interface `T$10` {
    var nearest: Any
    var center: Any
    var farthest: Any
}

external interface `T$11` {
    var x1: Number
    var x2: Number
    var y1: Number
    var y2: Number
}

external interface `T$12` {
    var tl: Point
    var mt: Point
    var tr: Point
    var ml: Point
    var mr: Point
    var bl: Point
    var mb: Point
    var br: Point
    var mtr: Point
}

external interface `T$13` {
    var bl: Point
    var br: Point
    var tl: Point
    var tr: Point
}

external interface IObjectOptions {
    var type: String? get() = definedExternally; set(value) = definedExternally
    var originX: String? get() = definedExternally; set(value) = definedExternally
    var originY: String? get() = definedExternally; set(value) = definedExternally
    var top: Number? get() = definedExternally; set(value) = definedExternally
    var left: Number? get() = definedExternally; set(value) = definedExternally
    var width: Number? get() = definedExternally; set(value) = definedExternally
    var height: Number? get() = definedExternally; set(value) = definedExternally
    var scaleX: Number? get() = definedExternally; set(value) = definedExternally
    var scaleY: Number? get() = definedExternally; set(value) = definedExternally
    var flipX: Boolean? get() = definedExternally; set(value) = definedExternally
    var flipY: Boolean? get() = definedExternally; set(value) = definedExternally
    var opacity: Number? get() = definedExternally; set(value) = definedExternally
    var angle: Number? get() = definedExternally; set(value) = definedExternally
    var skewX: Number? get() = definedExternally; set(value) = definedExternally
    var skewY: Number? get() = definedExternally; set(value) = definedExternally
    var cornerSize: Number? get() = definedExternally; set(value) = definedExternally
    var transparentCorners: Boolean? get() = definedExternally; set(value) = definedExternally
    var hoverCursor: String? get() = definedExternally; set(value) = definedExternally
    var moveCursor: String? get() = definedExternally; set(value) = definedExternally
    var padding: Number? get() = definedExternally; set(value) = definedExternally
    var borderColor: String? get() = definedExternally; set(value) = definedExternally
    var borderDashArray: Array<Number>? get() = definedExternally; set(value) = definedExternally
    var cornerColor: String? get() = definedExternally; set(value) = definedExternally
    var cornerStrokeColor: String? get() = definedExternally; set(value) = definedExternally
    var cornerStyle: dynamic /* "rect" | "circle" */ get() = definedExternally; set(value) = definedExternally
    var cornerDashArray: Array<Number>? get() = definedExternally; set(value) = definedExternally
    var centeredScaling: Boolean? get() = definedExternally; set(value) = definedExternally
    var centeredRotation: Boolean? get() = definedExternally; set(value) = definedExternally
    var fill: dynamic /* String | Pattern */ get() = definedExternally; set(value) = definedExternally
    var fillRule: String? get() = definedExternally; set(value) = definedExternally
    var globalCompositeOperation: String? get() = definedExternally; set(value) = definedExternally
    var backgroundColor: String? get() = definedExternally; set(value) = definedExternally
    var selectionBackgroundColor: String? get() = definedExternally; set(value) = definedExternally
    var stroke: String? get() = definedExternally; set(value) = definedExternally
    var strokeWidth: Number? get() = definedExternally; set(value) = definedExternally
    var strokeDashArray: Array<Number>? get() = definedExternally; set(value) = definedExternally
    var strokeDashOffset: Number? get() = definedExternally; set(value) = definedExternally
    var strokeLineCap: String? get() = definedExternally; set(value) = definedExternally
    var strokeLineJoin: String? get() = definedExternally; set(value) = definedExternally
    var strokeMiterLimit: Number? get() = definedExternally; set(value) = definedExternally
    var shadow: dynamic /* Shadow | String */ get() = definedExternally; set(value) = definedExternally
    var borderOpacityWhenMoving: Number? get() = definedExternally; set(value) = definedExternally
    var borderScaleFactor: Number? get() = definedExternally; set(value) = definedExternally
    var transformMatrix: Array<Any>? get() = definedExternally; set(value) = definedExternally
    var minScaleLimit: Number? get() = definedExternally; set(value) = definedExternally
    var selectable: Boolean? get() = definedExternally; set(value) = definedExternally
    var evented: Boolean? get() = definedExternally; set(value) = definedExternally
    var visible: Boolean? get() = definedExternally; set(value) = definedExternally
    var hasControls: Boolean? get() = definedExternally; set(value) = definedExternally
    var hasBorders: Boolean? get() = definedExternally; set(value) = definedExternally
    var hasRotatingPoint: Boolean? get() = definedExternally; set(value) = definedExternally
    var rotatingPointOffset: Number? get() = definedExternally; set(value) = definedExternally
    var perPixelTargetFind: Boolean? get() = definedExternally; set(value) = definedExternally
    var includeDefaultValues: Boolean? get() = definedExternally; set(value) = definedExternally
    var clipTo: Function<*>? get() = definedExternally; set(value) = definedExternally
    var lockMovementX: Boolean? get() = definedExternally; set(value) = definedExternally
    var lockMovementY: Boolean? get() = definedExternally; set(value) = definedExternally
    var lockRotation: Boolean? get() = definedExternally; set(value) = definedExternally
    var lockScalingX: Boolean? get() = definedExternally; set(value) = definedExternally
    var lockScalingY: Boolean? get() = definedExternally; set(value) = definedExternally
    var lockUniScaling: Boolean? get() = definedExternally; set(value) = definedExternally
    var lockSkewingX: Boolean? get() = definedExternally; set(value) = definedExternally
    var lockSkewingY: Boolean? get() = definedExternally; set(value) = definedExternally
    var lockScalingFlip: Boolean? get() = definedExternally; set(value) = definedExternally
    var excludeFromExport: Boolean? get() = definedExternally; set(value) = definedExternally
    var objectCaching: Boolean? get() = definedExternally; set(value) = definedExternally
    var statefullCache: Boolean? get() = definedExternally; set(value) = definedExternally
    var noScaleCache: Boolean? get() = definedExternally; set(value) = definedExternally
    var strokeUniform: Boolean? get() = definedExternally; set(value) = definedExternally
    var dirty: Boolean? get() = definedExternally; set(value) = definedExternally
    var paintFirst: String? get() = definedExternally; set(value) = definedExternally
    var stateProperties: Array<String>? get() = definedExternally; set(value) = definedExternally
    var cacheProperties: Array<String>? get() = definedExternally; set(value) = definedExternally
    var clipPath: Any? get() = definedExternally; set(value) = definedExternally
    var inverted: Boolean? get() = definedExternally; set(value) = definedExternally
    var absolutePositioned: Boolean? get() = definedExternally; set(value) = definedExternally
    var name: String? get() = definedExternally; set(value) = definedExternally
    var data: Any? get() = definedExternally; set(value) = definedExternally
    var oCoords: `T$12`? get() = definedExternally; set(value) = definedExternally
    var aCoords: `T$13`? get() = definedExternally; set(value) = definedExternally
    var matrixCache: Any? get() = definedExternally; set(value) = definedExternally
    var ownMatrixCache: Any? get() = definedExternally; set(value) = definedExternally
    var snapAngle: Number? get() = definedExternally; set(value) = definedExternally
    var snapThreshold: Number? get() = definedExternally; set(value) = definedExternally
    var group: Group? get() = definedExternally; set(value) = definedExternally
    var canvas: Canvas? get() = definedExternally; set(value) = definedExternally
}

open external class Object(options: IObjectOptions? = definedExternally /* null */) : IObservable<Any>, IObjectOptions,
    IObjectAnimation<Any> {
    override fun on(eventName: String, handler: (e: IEvent) -> Unit): Any = definedExternally

    override fun on(events: `T$2`): Any = definedExternally

    override fun trigger(eventName: String, options: Any?): Any = definedExternally

    override fun off(eventName: String?, handler: ((e: IEvent) -> Unit)?): Any = definedExternally

    override fun off(eventName: Any?, handler: ((e: IEvent) -> Unit)?): Any = definedExternally

    override fun off(): Any = definedExternally

    override fun animate(property: String, value: Number, options: IAnimationOptions?): Any = definedExternally

    override fun animate(property: String, value: String, options: IAnimationOptions?): Any = definedExternally

    override fun animate(properties: Any, options: IAnimationOptions?): Any = definedExternally

    open fun initialize(options: IObjectOptions? = definedExternally /* null */): Any = definedExternally
    open fun setOptions(options: IObjectOptions): Unit = definedExternally
    open fun transform(ctx: CanvasRenderingContext2D): Unit = definedExternally
    open fun toObject(propertiesToInclude: Array<String>? = definedExternally /* null */): Any = definedExternally
    open fun toDatalessObject(propertiesToInclude: Array<String>? = definedExternally /* null */): Any =
        definedExternally

    override fun toString(): String = definedExternally
    open fun getObjectScaling(): `T$14` = definedExternally
    open fun getTotalObjectScaling(): `T$14` = definedExternally
    open fun getObjectOpacity(): Number = definedExternally
    open fun setOnGroup(): Unit = definedExternally
    open fun getViewportTransform(): Array<Any> = definedExternally
    open fun render(ctx: CanvasRenderingContext2D): Unit = definedExternally
    open fun needsItsOwnCache(): Boolean = definedExternally
    open fun shouldCache(): Boolean = definedExternally
    open fun willDrawShadow(): Boolean = definedExternally
    open fun drawClipPathOnCache(ctx: CanvasRenderingContext2D): Unit = definedExternally
    open fun drawObject(ctx: CanvasRenderingContext2D): Unit = definedExternally
    open fun drawCacheOnCanvas(ctx: CanvasRenderingContext2D): Unit = definedExternally
    open fun isCacheDirty(skipCanvas: Boolean? = definedExternally /* null */): Boolean = definedExternally
    open fun clone(callback: Function<*>, propertiesToInclude: Array<String>? = definedExternally /* null */): Unit =
        definedExternally

    open fun cloneAsImage(callback: Function<*>, options: IDataURLOptions? = definedExternally /* null */): Any =
        definedExternally

    open fun toCanvasElement(options: IDataURLOptions? = definedExternally /* null */): String = definedExternally
    open fun toDataURL(options: IDataURLOptions): String = definedExternally
    open fun isType(type: String): Boolean = definedExternally
    open fun complexity(): Number = definedExternally
    open fun toJSON(propertiesToInclude: Array<String>? = definedExternally /* null */): Any = definedExternally
    open fun setGradient(
        property: String /* "stroke" */,
        options: IGradientOptions? = definedExternally /* null */
    ): Any = definedExternally

    open fun setGradient(
        property: String /* "fill" */,
        options: IGradientOptions? = definedExternally /* null */
    ): Any = definedExternally

    open fun setPatternFill(options: IFillOptions, callback: Function<*>): Any = definedExternally
    open fun setShadow(options: String? = definedExternally /* null */): Any = definedExternally
    open fun setShadow(options: Shadow? = definedExternally /* null */): Any = definedExternally
    open fun setColor(color: String): Any = definedExternally
    open fun rotate(angle: Number): Any = definedExternally
    open fun centerH(): Any = definedExternally
    open fun viewportCenterH(): Any = definedExternally
    open fun centerV(): Any = definedExternally
    open fun viewportCenterV(): Any = definedExternally
    open fun center(): Any = definedExternally
    open fun viewportCenter(): Any = definedExternally
    open fun getLocalPointer(e: Event, pointer: `T$4`? = definedExternally /* null */): `T$4` = definedExternally
    open fun <K : Any> get(property: K): Any = definedExternally
    open fun <K : Any> set(key: K, value: Any): Any = definedExternally
    open fun <K : Any> set(key: K, value: (value: Any) -> Any): Any = definedExternally
//    open fun set(options: Partial<Object /* this */>): Any = definedExternally
    open fun toggle(property: Any): Any = definedExternally
    open fun setSourcePath(value: String): Any = definedExternally
    open fun setAngle(angle: Number): Any = definedExternally
    open fun setOptions(options: Any? = definedExternally /* null */): Unit = definedExternally
    open fun setSourcePath(value: String): Any = definedExternally
    open fun getSvgStyles(skipShadow: Boolean? = definedExternally /* null */): String = definedExternally
    open fun getSvgTransform(
        full: Boolean? = definedExternally /* null */,
        additionalTransform: String? = definedExternally /* null */
    ): String = definedExternally

    open fun getSvgTransformMatrix(): String = definedExternally
    open fun hasStateChanged(propertySet: String): Boolean = definedExternally
    open fun saveState(options: `T$15`? = definedExternally /* null */): Any = definedExternally
    open fun setupState(options: Any? = definedExternally /* null */): Any = definedExternally
    open fun straighten(): Any = definedExternally
    open fun fxStraighten(callbacks: Callbacks): Any = definedExternally
    open fun bringForward(intersecting: Boolean? = definedExternally /* null */): Any = definedExternally
    open fun bringToFront(): Any = definedExternally
    open fun sendBackwards(intersecting: Boolean? = definedExternally /* null */): Any = definedExternally
    open fun sendToBack(): Any = definedExternally
    open fun moveTo(index: Number): Any = definedExternally
    open fun translateToCenterPoint(point: Point, originX: String, originY: String): Point = definedExternally
    open fun translateToOriginPoint(center: Point, originX: String, originY: String): Point = definedExternally
    open fun getCenterPoint(): Point = definedExternally
    open fun getPointByOrigin(originX: String, originY: String): Point = definedExternally
    open fun toLocalPoint(point: Point, originX: String, originY: String): Point = definedExternally
    open fun setPositionByOrigin(pos: Point, originX: String, originY: String): Unit = definedExternally
    open fun adjustPosition(to: String): Unit = definedExternally
    open fun drawBorders(ctx: CanvasRenderingContext2D, styleOverride: Any? = definedExternally /* null */): Any =
        definedExternally

    open fun drawBordersInGroup(
        ctx: CanvasRenderingContext2D,
        options: Any? = definedExternally /* null */,
        styleOverride: Any? = definedExternally /* null */
    ): Any = definedExternally

    open fun drawControls(ctx: CanvasRenderingContext2D, styleOverride: Any? = definedExternally /* null */): Any =
        definedExternally

    open fun drawSelectionBackground(ctx: CanvasRenderingContext2D): Any = definedExternally
    open fun drawCorners(context: CanvasRenderingContext2D): Any = definedExternally
    open fun isControlVisible(controlName: String): Boolean = definedExternally
    open fun setControlVisible(controlName: String, visible: Boolean): Any = definedExternally
    open fun setControlsVisibility(options: `T$16`? = definedExternally /* null */): Object /* this */ =
        definedExternally

    open fun setCoords(
        ignoreZoom: Boolean? = definedExternally /* null */,
        skipAbsolute: Boolean? = definedExternally /* null */
    ): Any = definedExternally

    open fun getBoundingRect(
        absolute: Boolean? = definedExternally /* null */,
        calculate: Boolean? = definedExternally /* null */
    ): `T$17` = definedExternally

    open fun isContainedWithinObject(
        other: Any,
        absolute: Boolean? = definedExternally /* null */,
        calculate: Boolean? = definedExternally /* null */
    ): Boolean = definedExternally

    open fun isContainedWithinRect(
        pointTL: Any,
        pointBR: Any,
        absolute: Boolean? = definedExternally /* null */,
        calculate: Boolean? = definedExternally /* null */
    ): Boolean = definedExternally

    open fun containsPoint(
        point: Point,
        lines: Any? = definedExternally /* null */,
        absolute: Boolean? = definedExternally /* null */,
        calculate: Boolean? = definedExternally /* null */
    ): Boolean = definedExternally

    open fun scale(value: Number): Any = definedExternally
    open fun scaleToHeight(value: Number, absolute: Boolean? = definedExternally /* null */): Any = definedExternally
    open fun scaleToWidth(value: Number, absolute: Boolean? = definedExternally /* null */): Any = definedExternally
    open fun intersectsWithObject(
        other: Any,
        absolute: Boolean? = definedExternally /* null */,
        calculate: Boolean? = definedExternally /* null */
    ): Boolean = definedExternally

    open fun intersectsWithRect(
        pointTL: Any,
        pointBR: Any,
        absolute: Boolean? = definedExternally /* null */,
        calculate: Boolean? = definedExternally /* null */
    ): Boolean = definedExternally

    open fun animate(): Any = definedExternally
    open fun calcCoords(absolute: Boolean? = definedExternally /* null */): Any = definedExternally
    open fun calcTransformMatrix(skipGroup: Boolean? = definedExternally /* null */): Array<Any> = definedExternally
    open fun getCoords(
        absolute: Boolean? = definedExternally /* null */,
        calculate: Boolean? = definedExternally /* null */
    ): Any = definedExternally

    open fun getScaledHeight(): Number = definedExternally
    open fun getScaledWidth(): Number = definedExternally
    open fun getSvgCommons(): String = definedExternally
    open fun getSvgFilter(): String = definedExternally
    open fun getSvgSpanStyles(style: Any, useWhiteSpace: Boolean? = definedExternally /* null */): String =
        definedExternally

    open fun getSvgTextDecoration(style: Any): String = definedExternally
    open fun isOnScreen(calculate: Boolean? = definedExternally /* null */): Boolean = definedExternally
    open fun isPartiallyOnScreen(calculate: Boolean? = definedExternally /* null */): Boolean = definedExternally
    open fun onDeselect(options: `T$18`): Boolean = definedExternally
    open fun onSelect(): Unit = definedExternally
    open fun toClipPathSVG(reviver: Function<*>? = definedExternally /* null */): String = definedExternally
    open fun toSVG(reviver: Function<*>? = definedExternally /* null */): String = definedExternally
    open fun translateToGivenOrigin(
        pointL: Point,
        fromOriginX: String,
        fromOriginY: String,
        toOriginX: String,
        toOriginY: String
    ): Point = definedExternally

    open fun _getNonTransformedDimensions(): `T$4` = definedExternally
    open fun _getLeftTopCoords(): Point = definedExternally
    open fun _getTransformedDimensions(
        skewX: Number? = definedExternally /* null */,
        skewY: Number? = definedExternally /* null */
    ): `T$4` = definedExternally

    open fun _renderFill(ctx: CanvasRenderingContext2D): Unit = definedExternally
    open fun _renderStroke(ctx: CanvasRenderingContext2D): Unit = definedExternally
    open fun _removeShadow(ctx: CanvasRenderingContext2D): Unit = definedExternally
    open fun _setLineDash(
        ctx: CanvasRenderingContext2D,
        dashArray: Array<Number>,
        alternative: ((ctx: CanvasRenderingContext2D) -> Unit)? = definedExternally /* null */
    ): Unit = definedExternally

    open fun _applyPatternGradientTransform(ctx: CanvasRenderingContext2D, filler: String): Unit = definedExternally
    open fun _applyPatternGradientTransform(ctx: CanvasRenderingContext2D, filler: Pattern): Unit = definedExternally
    open fun _applyPatternGradientTransform(ctx: CanvasRenderingContext2D, filler: Gradient): Unit = definedExternally
    open fun _render(ctx: CanvasRenderingContext2D): Unit = definedExternally
    open fun _renderPaintInOrder(ctx: CanvasRenderingContext2D): Unit = definedExternally
    open fun _getControlsVisibility(): `T$19` = definedExternally
    open fun setShadow(): Any = definedExternally
    open fun setSourcePath(value: String): Any = definedExternally

    companion object {
        fun _fromObject(
            className: String,
            `object`: Any,
            callback: Function<*>? = definedExternally /* null */,
            extraParam: Any? = definedExternally /* null */
        ): Any = definedExternally
    }
}

external interface `T$14` {
    var scaleX: Number
    var scaleY: Number
}

external interface `T$15` {
    var stateProperties: Array<Any>? get() = definedExternally; set(value) = definedExternally
    var propertySet: String? get() = definedExternally; set(value) = definedExternally
}

external interface `T$16` {
    var bl: Boolean? get() = definedExternally; set(value) = definedExternally
    var br: Boolean? get() = definedExternally; set(value) = definedExternally
    var mb: Boolean? get() = definedExternally; set(value) = definedExternally
    var ml: Boolean? get() = definedExternally; set(value) = definedExternally
    var mr: Boolean? get() = definedExternally; set(value) = definedExternally
    var mt: Boolean? get() = definedExternally; set(value) = definedExternally
    var tl: Boolean? get() = definedExternally; set(value) = definedExternally
    var tr: Boolean? get() = definedExternally; set(value) = definedExternally
    var mtr: Boolean? get() = definedExternally; set(value) = definedExternally
}

external interface `T$17` {
    var left: Number
    var top: Number
    var width: Number
    var height: Number
}

external interface `T$18` {
    var e: Event? get() = definedExternally; set(value) = definedExternally
    var `object`: Any? get() = definedExternally; set(value) = definedExternally
}

external interface `T$19` {
    var tl: Boolean
    var tr: Boolean
    var br: Boolean
    var bl: Boolean
    var ml: Boolean
    var mt: Boolean
    var mr: Boolean
    var mb: Boolean
    var mtr: Boolean
}

external interface IPathOptions : IObjectOptions {
    var path: Array<Point>? get() = definedExternally; set(value) = definedExternally
}

open external class Path : Object, IPathOptions {
    constructor(path: String?, options: IPathOptions?)
    constructor(path: Array<Point>?, options: IPathOptions?)

    open var pathOffset: Point = definedExternally
//    override fun toClipPathSVG(reviver: Function<*>? /* null */): String = definedExternally
//    override fun toSVG(reviver: Function<*>? /* null */): String = definedExternally

    companion object {
        fun fromElement(
            element: SVGElement,
            callback: Function<*>,
            options: IPathOptions? = definedExternally /* null */
        ): Path = definedExternally

        fun fromObject(`object`: Any, callback: Function<*>): Path = definedExternally
        var ATTRIBUTE_NAMES: Array<String> = definedExternally
    }
}

open external class Polygon(points: Array<`T$4`>, options: IPolylineOptions? = definedExternally /* null */) : /*Polyline,*/
    IPolylineOptions {
    companion object {
        fun fromElement(element: SVGElement, options: IPolylineOptions?): Polygon = definedExternally
        fun fromObject(`object`: Any): Polygon = definedExternally
    }
}

external interface IPolylineOptions : IObjectOptions {
    var points: Array<Point>? get() = definedExternally; set(value) = definedExternally
}

open external class Polyline(points: Array<`T$4`>, options: IPolylineOptions? = definedExternally /* null */) : /*Object,*/
    IPolylineOptions {
    open var pathOffset: Point = definedExternally
    open fun _calcDimensions(): `T$17` = definedExternally

    companion object {
        var ATTRIBUTE_NAMES: Array<String> = definedExternally
        fun fromElement(element: SVGElement, options: IPolylineOptions? = definedExternally /* null */): Polyline =
            definedExternally

        fun fromObject(`object`: Any): Polyline = definedExternally
    }
}

external interface IRectOptions : IObjectOptions {
    var rx: Number? get() = definedExternally; set(value) = definedExternally
    var ry: Number? get() = definedExternally; set(value) = definedExternally
}

open external class Rect(options: IRectOptions? = definedExternally /* null */) : Object, IRectOptions {
    companion object {
        var ATTRIBUTE_NAMES: Array<String> = definedExternally
        fun fromElement(element: SVGElement, options: IRectOptions? = definedExternally /* null */): Rect =
            definedExternally

        fun fromObject(`object`: Any): Rect = definedExternally
    }
}

external interface `T$20` {
    var size: Number
    var baseline: Number
}

external interface `T$21` {
    var width: Number
    var left: Number
    var height: Number
    var kernedWidth: Number
    var deltaY: Number
}

external interface TextOptions : IObjectOptions {
    override var type: String? get() = definedExternally; set(value) = definedExternally
    var fontSize: Number? get() = definedExternally; set(value) = definedExternally
    var fontWeight: dynamic /* String | Number */ get() = definedExternally; set(value) = definedExternally
    var fontFamily: String? get() = definedExternally; set(value) = definedExternally
    var underline: Boolean? get() = definedExternally; set(value) = definedExternally
    var overline: Boolean? get() = definedExternally; set(value) = definedExternally
    var linethrough: Boolean? get() = definedExternally; set(value) = definedExternally
    var textAlign: String? get() = definedExternally; set(value) = definedExternally
    var fontStyle: dynamic /* '' | 'normal' | 'italic' | 'oblique' */ get() = definedExternally; set(value) = definedExternally
    var lineHeight: Number? get() = definedExternally; set(value) = definedExternally
    var superscript: `T$20`? get() = definedExternally; set(value) = definedExternally
    var subscript: `T$20`? get() = definedExternally; set(value) = definedExternally
    var textBackgroundColor: String? get() = definedExternally; set(value) = definedExternally
    override var stroke: String? get() = definedExternally; set(value) = definedExternally
    override var shadow: dynamic /* Shadow | String */ get() = definedExternally; set(value) = definedExternally
    var charSpacing: Number? get() = definedExternally; set(value) = definedExternally
    var styles: Any? get() = definedExternally; set(value) = definedExternally
    var deltaY: Number? get() = definedExternally; set(value) = definedExternally
    var text: String? get() = definedExternally; set(value) = definedExternally
    override var cacheProperties: Array<String>? get() = definedExternally; set(value) = definedExternally
    override var stateProperties: Array<String>? get() = definedExternally; set(value) = definedExternally
    var __charBounds: Array<Array<`T$21`>>? get() = definedExternally; set(value) = definedExternally
}

open external class Text(text: String, options: TextOptions? = definedExternally /* null */) : Object, TextOptions {
    open var textLines: Array<String> = definedExternally
    open var _textLines: Array<Array<String>> = definedExternally
    open var _unwrappedTextLines: Array<Array<String>> = definedExternally
    open var _reSpacesAndTabs: RegExp = definedExternally
    open var _reSpaceAndTab: RegExp = definedExternally
    open var __lineHeights: Array<Number> = definedExternally
    open var _fontSizeMult: Number = definedExternally
    open var _fontSizeFraction: Number = definedExternally
    open var __skipDimension: Boolean = definedExternally
    open fun getMeasuringContext(): CanvasRenderingContext2D = definedExternally
    open fun initDimensions(): Unit = definedExternally
    open fun enlargeSpaces(): Unit = definedExternally
    open fun isEndOfWrapping(lineIndex: Number): Boolean = definedExternally
    override fun toString(): String = definedExternally
    open fun getHeightOfChar(line: Number, char: Number): Number = definedExternally
    open fun measureLine(lineIndex: Number): `T$22` = definedExternally
    open fun getHeightOfLine(lineIndex: Number): Number = definedExternally
    open fun calcTextHeight(): Number = definedExternally
    open fun setSuperscript(start: Number, end: Number): Text = definedExternally
    open fun setSubscript(start: Number, end: Number): Text = definedExternally
    open fun getValueOfPropertyAt(lineIndex: Number, charIndex: Number, property: String): Any = definedExternally
    open fun cleanStyle(property: String): Unit = definedExternally
    open fun get2DCursorLocation(selectionStart: Number, skipWrapping: Boolean): `T$23` = definedExternally
    open fun getCompleteStyleDeclaration(lineIndex: Number, charIndex: Number): Any = definedExternally
    open fun getSelectionStyles(
        startIndex: Number? = definedExternally /* null */,
        endIndex: Number? = definedExternally /* null */,
        complete: Boolean? = definedExternally /* null */
    ): Array<Any> = definedExternally

//    override fun getSvgStyles(skipShadow: Boolean?): String = definedExternally
    open fun isEmptyStyles(lineIndex: Number): Boolean = definedExternally
    open fun removeStyle(property: String): Unit = definedExternally
    open fun setSelectionStyles(
        styles: Any,
        startIndex: Number? = definedExternally /* null */,
        endIndex: Number? = definedExternally /* null */
    ): Text = definedExternally

    open fun styleHas(property: String, lineIndex: Number? = definedExternally /* null */): Boolean = definedExternally
    open fun getLineWidth(lineIndex: Number): Number = definedExternally
    open fun _getLineLeftOffset(lineIndex: Number): Number = definedExternally
    open fun _applyCharStyles(
        method: String,
        ctx: CanvasRenderingContext2D,
        lineIndex: Number,
        charIndex: Number,
        styleDeclaration: Any
    ): Unit = definedExternally

    open fun _getStyleDeclaration(lineIndex: Number, charIndex: Number): Any = definedExternally
    open fun _generateStyleMap(textInfo: `T$24`): Array<`T$25`> = definedExternally
    open fun _getWidthOfCharSpacing(): Number = definedExternally
    open fun _measureChar(_char: String, charStyle: Any, previousChar: String, prevCharStyle: Any): `T$26` =
        definedExternally

    open fun _renderChars(
        method: String,
        ctx: CanvasRenderingContext2D,
        line: String,
        left: Number,
        top: Number,
        lineIndex: Number
    ): Unit = definedExternally

    open fun _renderChar(
        method: String,
        ctx: CanvasRenderingContext2D,
        lineIndex: Number,
        charIndex: Number,
        _char: String,
        left: Number,
        top: Number
    ): Unit = definedExternally

    open fun _clearCache(): Unit = definedExternally
    open fun _splitText(): `T$24` = definedExternally
    open fun _hasStyleChanged(prevStyle: Any, thisStyle: Any): Boolean = definedExternally

    companion object {
        var DEFAULT_SVG_FONT_SIZE: Number = definedExternally
        fun fromElement(
            element: SVGElement,
            callback: Function<*>? = definedExternally /* null */,
            options: TextOptions? = definedExternally /* null */
        ): Text = definedExternally

        fun fromObject(`object`: Any, callback: Function<*>? = definedExternally /* null */): Text = definedExternally
    }
}

external interface `T$22` {
    var width: Number
    var numOfSpaces: Number
}

external interface `T$23` {
    var lineIndex: Number
    var charIndex: Number
}

external interface `T$24` {
    var _unwrappedLines: Array<String>
    var lines: Array<String>
    var graphemeText: Array<String>
    var graphemeLines: Array<String>
}

external interface `T$25` {
    var line: Number
    var offset: Number
}

external interface `T$26` {
    var width: Number
    var kernedWidth: Number
}

external interface ITextOptions : TextOptions {
    var selectionStart: Number? get() = definedExternally; set(value) = definedExternally
    var selectionEnd: Number? get() = definedExternally; set(value) = definedExternally
    var selectionColor: String? get() = definedExternally; set(value) = definedExternally
    var selected: Boolean? get() = definedExternally; set(value) = definedExternally
    var isEditing: Boolean? get() = definedExternally; set(value) = definedExternally
    var editable: Boolean? get() = definedExternally; set(value) = definedExternally
    var editingBorderColor: String? get() = definedExternally; set(value) = definedExternally
    var cursorWidth: Number? get() = definedExternally; set(value) = definedExternally
    var cursorColor: String? get() = definedExternally; set(value) = definedExternally
    var cursorDelay: Number? get() = definedExternally; set(value) = definedExternally
    var cursorDuration: Number? get() = definedExternally; set(value) = definedExternally
    var caching: Boolean? get() = definedExternally; set(value) = definedExternally
    var inCompositionMode: Boolean? get() = definedExternally; set(value) = definedExternally
    var path: String? get() = definedExternally; set(value) = definedExternally
    var useNative: Boolean? get() = definedExternally; set(value) = definedExternally
    var ctrlKeysMapDown: Any? get() = definedExternally; set(value) = definedExternally
    var ctrlKeysMapUp: Any? get() = definedExternally; set(value) = definedExternally
    var keysMap: Any? get() = definedExternally; set(value) = definedExternally
    var hiddenTextarea: HTMLTextAreaElement? get() = definedExternally; set(value) = definedExternally
}

open external class IText(text: String, options: ITextOptions? = definedExternally /* null */) : Text, ITextOptions {
    open fun setSelectionStart(index: Number): Unit = definedExternally
    open fun setSelectionEnd(index: Number): Unit = definedExternally
    open fun clearContextTop(skipRestore: Boolean? = definedExternally /* null */): Unit = definedExternally
    open fun renderCursorOrSelection(): Unit = definedExternally
    open fun renderCursor(boundaries: Any, ctx: CanvasRenderingContext2D): Unit = definedExternally
    open fun renderSelection(boundaries: Any, ctx: CanvasRenderingContext2D): Unit = definedExternally
    open fun getCurrentCharFontSize(): Number = definedExternally
    open fun getCurrentCharColor(): String = definedExternally
    open fun initBehavior(): Unit = definedExternally
    open fun initAddedHandler(): Unit = definedExternally
    open fun initDelayedCursor(): Unit = definedExternally
    open fun abortCursorAnimation(): Unit = definedExternally
    open fun selectAll(): IText = definedExternally
    open fun getSelectedText(): String = definedExternally
    open fun findWordBoundaryLeft(startFrom: Number): Number = definedExternally
    open fun findWordBoundaryRight(startFrom: Number): Number = definedExternally
    open fun findLineBoundaryLeft(startFrom: Number): Number = definedExternally
    open fun findLineBoundaryRight(startFrom: Number): Number = definedExternally
    open fun searchWordBoundary(selectionStart: Number, direction: Number): Number = definedExternally
    open fun selectWord(selectionStart: Number): Unit = definedExternally
    open fun selectLine(selectionStart: Number): IText = definedExternally
    open fun enterEditing(): IText = definedExternally
    open fun initMouseMoveHandler(): Unit = definedExternally
    open fun exitEditing(): IText = definedExternally
    open fun removeStyleFromTo(start: Number, end: Number): Unit = definedExternally
    open fun shiftLineStyles(lineIndex: Number, offset: Number): Unit = definedExternally
    open fun insertNewlineStyleObject(
        lineIndex: Number,
        charIndex: Number,
        qty: Number,
        copiedStyle: Array<Any>
    ): Unit = definedExternally

    open fun insertCharStyleObject(
        lineIndex: Number,
        charIndex: Number,
        quantity: Number,
        copiedStyle: Array<Any>
    ): Unit = definedExternally

    open fun insertNewStyleBlock(
        insertedText: Array<Any>,
        start: Number,
        copiedStyle: Array<Any>? = definedExternally /* null */
    ): Unit = definedExternally

    open fun setSelectionStartEndWithShift(start: Number, end: Number, newSelection: Number): Unit = definedExternally
    open fun copy(): Unit = definedExternally
    open fun fromGraphemeToStringSelection(start: Number, end: Number, _text: String): `T$27` = definedExternally
    open fun fromStringToGraphemeSelection(start: Number, end: Number, text: String): `T$27` = definedExternally
    open fun getDownCursorOffset(e: Event, isRight: Boolean? = definedExternally /* null */): Number = definedExternally
    open fun getSelectionStartFromPointer(e: Event): Number = definedExternally
    open fun getUpCursorOffset(e: Event, isRight: Boolean? = definedExternally /* null */): Number = definedExternally
    open fun initClicks(): Unit = definedExternally
    open fun initCursorSelectionHandlers(): Unit = definedExternally
    open fun initDoubleClickSimulation(): Unit = definedExternally
    open fun initHiddenTextarea(): Unit = definedExternally
    open fun initMousedownHandler(): Unit = definedExternally
    open fun initMouseupHandler(): Unit = definedExternally
    open fun insertChars(text: String, style: Array<Any>, start: Number, end: Number): Unit = definedExternally
    open fun moveCursorDown(e: Event): Unit = definedExternally
    open fun moveCursorLeft(e: Event): Unit = definedExternally
    open fun moveCursorLeftWithoutShift(e: Event): Unit = definedExternally
    open fun moveCursorLeftWithShift(e: Event): Unit = definedExternally
    open fun moveCursorRight(e: Event): Unit = definedExternally
    open fun moveCursorRightWithoutShift(e: Event): Unit = definedExternally
    open fun moveCursorRightWithShift(e: Event): Unit = definedExternally
    open fun moveCursorUp(e: Event): Unit = definedExternally
    open fun moveCursorWithoutShift(offset: Number): Unit = definedExternally
    open fun moveCursorWithShift(offset: Number): Unit = definedExternally
    open fun onCompositionEnd(): Unit = definedExternally
    open fun onCompositionStart(): Unit = definedExternally
    open fun onInput(e: Event): Unit = definedExternally
    open fun onKeyDown(e: Event): Unit = definedExternally
    open fun onKeyUp(e: Event): Unit = definedExternally
    open fun paste(): Unit = definedExternally
    open fun removeChars(start: Number, end: Number): Unit = definedExternally
    open fun setCursorByClick(e: Event): Unit = definedExternally
    open fun _getNewSelectionStartFromOffset(
        mouseOffset: `T$4`,
        prevWidth: Number,
        width: Number,
        index: Number,
        jlen: Number
    ): Number = definedExternally

//    override fun _render(ctx: CanvasRenderingContext2D): Unit = definedExternally
    open fun _updateTextarea(): Unit = definedExternally
    open fun _mouseDownHandler(options: Any): Unit = definedExternally
    open fun set(property: String, value: Any): Unit = definedExternally

    companion object {
//        override fun fromObject(`object`: Any, callback: Function<*>?): IText = definedExternally
    }
}

external interface `T$27` {
    var selectionStart: String
    var selectionEnd: String
}

external interface ITextboxOptions : ITextOptions {
    var minWidth: Number? get() = definedExternally; set(value) = definedExternally
    var dynamicMinWidth: Number? get() = definedExternally; set(value) = definedExternally
    override var lockScalingFlip: Boolean? get() = definedExternally; set(value) = definedExternally
    override var noScaleCache: Boolean? get() = definedExternally; set(value) = definedExternally
    var splitByGrapheme: Boolean? get() = definedExternally; set(value) = definedExternally
    var isWrapping: Boolean? get() = definedExternally; set(value) = definedExternally
}

open external class Textbox(text: String, options: ITextboxOptions? = definedExternally /* null */) : /*IText,*/
    ITextboxOptions {
    open fun styleHas(property: String, lineIndex: Number): Boolean = definedExternally
//    override fun isEmptyStyles(lineIndex: Number): Boolean = definedExternally
//    override fun isEndOfWrapping(lineIndex: Number): Boolean = definedExternally
    open fun getMinWidth(): Number = definedExternally
    open var _wordJoiners: RegExp = definedExternally
    open fun _measureWord(word: Array<String>, lineIndex: Number, charOffset: Number): Number = definedExternally
    open fun _wrapText(lines: Array<String>, desiredWidth: Number): Array<String> = definedExternally
    open var _styleMap: Array<`T$25`> = definedExternally

    companion object {
//        override fun fromObject(`object`: Any, callback: Function<*>?): Textbox = definedExternally
    }
}

external interface ITriangleOptions : IObjectOptions
//open external class Triangle(options: ITriangleOptions? = definedExternally /* null */) : Object {
//    override fun toSVG(reviver: Function<*>?): String = definedExternally
//
//    companion object {
//        fun fromObject(`object`: Any): Triangle = definedExternally
//    }
//}

external interface `T$28` {
    fun fromObject(`object`: Any): IBlendColorFilter
}

external interface `T$29` {
    fun fromObject(`object`: Any): IBlendImageFilter
}

external interface `T$30` {
    fun fromObject(`object`: Any): IBrightnessFilter
}

external interface `T$31` {
    fun fromObject(`object`: Any): IColorMatrix
}

external interface `T$32` {
    fun fromObject(`object`: Any): IContrastFilter
}

external interface `T$33` {
    fun fromObject(`object`: Any): IConvoluteFilter
}

external interface `T$34` {
    fun fromObject(`object`: Any): IGradientTransparencyFilter
}

external interface `T$35` {
    fun fromObject(`object`: Any): IGrayscaleFilter
}

external interface `T$36` {
    fun fromObject(`object`: Any): IInvertFilter
}

external interface `T$37` {
    fun fromObject(`object`: Any): IMaskFilter
}

external interface `T$38` {
    fun fromObject(`object`: Any): IMultiplyFilter
}

external interface `T$39` {
    fun fromObject(`object`: Any): INoiseFilter
}

external interface `T$40` {
    fun fromObject(`object`: Any): IPixelateFilter
}

external interface `T$41` {
    fun fromObject(`object`: Any): IRemoveWhiteFilter
}

external interface `T$42` {
    fun fromObject(`object`: Any): IResizeFilter
}

external interface `T$43` {
    fun fromObject(`object`: Any): ISaturationFilter
}

external interface `T$44` {
    fun fromObject(`object`: Any): ISepia2Filter
}

external interface `T$45` {
    fun fromObject(`object`: Any): ISepiaFilter
}

external interface `T$46` {
    fun fromObject(`object`: Any): ITintFilter
}

external interface IAllFilters {
    var BaseFilter: Any
    var BlendColor: `T$28`
    var BlendImage: `T$29`
    var Brightness: `T$30`
    var ColorMatrix: `T$31`
    var Contrast: `T$32`
    var Convolute: `T$33`
    var GradientTransparency: `T$34`
    var Grayscale: `T$35`
    var Invert: `T$36`
    var Mask: `T$37`
    var Multiply: `T$38`
    var Noise: `T$39`
    var Pixelate: `T$40`
    var RemoveWhite: `T$41`
    var Resize: `T$42`
    var Saturation: `T$43`
    var Sepia2: `T$44`
    var Sepia: `T$45`
    var Tint: `T$46`
}

external interface IBaseFilter {
    fun setOptions(options: Any? = definedExternally /* null */)
    fun toObject(): Any
    fun toJSON(): String
}

external interface IBlendColorFilter : IBaseFilter {
    var color: String? get() = definedExternally; set(value) = definedExternally
    var mode: String? get() = definedExternally; set(value) = definedExternally
    var alpha: Number? get() = definedExternally; set(value) = definedExternally
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface IBlendImageFilter : IBaseFilter {
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface IBrightnessFilter : IBaseFilter {
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface IColorMatrix : IBaseFilter {
    var matrix: Array<Number>? get() = definedExternally; set(value) = definedExternally
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface IContrastFilter : IBaseFilter {
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface IConvoluteFilter : IBaseFilter {
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface IGradientTransparencyFilter : IBaseFilter {
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface IGrayscaleFilter : IBaseFilter {
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface IInvertFilter : IBaseFilter {
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface IMaskFilter : IBaseFilter {
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface IMultiplyFilter : IBaseFilter {
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface INoiseFilter : IBaseFilter {
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface IPixelateFilter : IBaseFilter {
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface IRemoveWhiteFilter : IBaseFilter {
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface IResizeFilter : IBaseFilter {
    var resizeType: String
    var scaleX: Number
    var scaleY: Number
    var lanczosLobes: Number
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface ISaturationFilter : IBaseFilter {
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface ISepiaFilter : IBaseFilter {
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface ISepia2Filter : IBaseFilter {
    fun applyTo(canvasEl: HTMLCanvasElement)
}

external interface ITintFilter : IBaseFilter {
    fun applyTo(canvasEl: HTMLCanvasElement)
}

open external class BaseBrush {
    open var color: String = definedExternally
    open var width: Number = definedExternally
    open var shadow: dynamic /* Shadow | String */ = definedExternally
    open var strokeLineCap: String = definedExternally
    open var strokeLineJoin: String = definedExternally
    open var strokeDashArray: Array<Any> = definedExternally
    open fun setShadow(options: String): BaseBrush = definedExternally
    open fun setShadow(options: Any): BaseBrush = definedExternally
}

open external class CircleBrush : BaseBrush {
    override var width: Number = definedExternally
    open fun drawDot(pointer: Any): Unit = definedExternally
    open fun addPoint(pointer: Any): Point = definedExternally
}

open external class SprayBrush : BaseBrush {
    override var width: Number = definedExternally
    open var density: Number = definedExternally
    open var dotWidth: Number = definedExternally
    open var dotWidthVariance: Number = definedExternally
    open var randomOpacity: Boolean = definedExternally
    open var optimizeOverlapping: Boolean = definedExternally
    open fun addSprayChunk(pointer: Any): Unit = definedExternally
}

open external class PatternBrush : PencilBrush {
    open fun getPatternSrc(): HTMLCanvasElement = definedExternally
    open fun getPatternSrcFunction(): String = definedExternally
    open fun getPattern(): Any = definedExternally
    override fun createPath(pathData: String): Path = definedExternally
}

open external class PencilBrush : BaseBrush {
    open fun convertPointsToSVGPath(
        points: Array<`T$4`>,
        minX: Number? = definedExternally /* null */,
        minY: Number? = definedExternally /* null */
    ): Array<String> = definedExternally

    open fun createPath(pathData: String): Path = definedExternally
}

external interface IUtilAnimationOptions {
    var startValue: Number? get() = definedExternally; set(value) = definedExternally
    var endValue: Number? get() = definedExternally; set(value) = definedExternally
    var byValue: Number
    var duration: Number? get() = definedExternally; set(value) = definedExternally
    var onChange: Function<*>? get() = definedExternally; set(value) = definedExternally
    var onComplete: Function<*>? get() = definedExternally; set(value) = definedExternally
    var easing: Function<*>? get() = definedExternally; set(value) = definedExternally
}

external interface IUtilAnimation {
    fun animate(options: IUtilAnimationOptions? = definedExternally /* null */)
    fun requestAnimFrame(callback: Function<*>)
}


external interface IUtilAnimEase {
    var easeInBack: IUtilAminEaseFunction
    var easeInBounce: IUtilAminEaseFunction
    var easeInCirc: IUtilAminEaseFunction
    var easeInCubic: IUtilAminEaseFunction
    var easeInElastic: IUtilAminEaseFunction
    var easeInExpo: IUtilAminEaseFunction
    var easeInOutBack: IUtilAminEaseFunction
    var easeInOutBounce: IUtilAminEaseFunction
    var easeInOutCirc: IUtilAminEaseFunction
    var easeInOutCubic: IUtilAminEaseFunction
    var easeInOutElastic: IUtilAminEaseFunction
    var easeInOutExpo: IUtilAminEaseFunction
    var easeInOutQuad: IUtilAminEaseFunction
    var easeInOutQuart: IUtilAminEaseFunction
    var easeInOutQuint: IUtilAminEaseFunction
    var easeInOutSine: IUtilAminEaseFunction
    var easeInQuad: IUtilAminEaseFunction
    var easeInQuart: IUtilAminEaseFunction
    var easeInQuint: IUtilAminEaseFunction
    var easeInSine: IUtilAminEaseFunction
    var easeOutBack: IUtilAminEaseFunction
    var easeOutBounce: IUtilAminEaseFunction
    var easeOutCirc: IUtilAminEaseFunction
    var easeOutCubic: IUtilAminEaseFunction
    var easeOutElastic: IUtilAminEaseFunction
    var easeOutExpo: IUtilAminEaseFunction
    var easeOutQuad: IUtilAminEaseFunction
    var easeOutQuart: IUtilAminEaseFunction
    var easeOutQuint: IUtilAminEaseFunction
    var easeOutSine: IUtilAminEaseFunction
}

external interface IUtilArc {
    fun drawArc(ctx: CanvasRenderingContext2D, fx: Number, fy: Number, coords: Array<Number>)
    fun getBoundsOfArc(
        fx: Number,
        fy: Number,
        rx: Number,
        ry: Number,
        rot: Number,
        large: Number,
        sweep: Number,
        tx: Number,
        ty: Number
    ): Array<Point>

    fun getBoundsOfCurve(
        x0: Number,
        y0: Number,
        x1: Number,
        y1: Number,
        x2: Number,
        y2: Number,
        x3: Number,
        y3: Number
    ): Array<Point>
}

external interface IUtilDomEvent {
    fun getPointer(event: Event, upperCanvasEl: HTMLCanvasElement): Point
    fun addListener(element: HTMLElement, eventName: String, handler: Function<*>)
    fun removeListener(element: HTMLElement, eventName: String, handler: Function<*>)
}

external interface `T$47` {
    var left: Number
    var right: Number
}

external interface IUtilDomMisc {
    fun getById(id: String): HTMLElement
    fun getById(id: HTMLElement): HTMLElement
    fun toArray(arrayLike: Any): Array<Any>
    fun makeElement(tagName: String, attributes: Any? = definedExternally /* null */): HTMLElement
    fun addClass(element: HTMLElement, classname: String)
    fun wrapElement(
        element: HTMLElement,
        wrapper: HTMLElement,
        attributes: Any? = definedExternally /* null */
    ): HTMLElement

    fun wrapElement(element: HTMLElement, wrapper: String, attributes: Any? = definedExternally /* null */): HTMLElement
    fun getScrollLeftTop(element: HTMLElement, upperCanvasEl: HTMLElement): `T$47`
    fun getElementOffset(element: HTMLElement): `T$47`
    fun getElementStyle(elment: HTMLElement, attr: String): String
    fun getScript(url: String, callback: Function<*>)
    fun makeElementUnselectable(element: HTMLElement): HTMLElement
    fun makeElementSelectable(element: HTMLElement): HTMLElement
}

external interface `T$48` {
    var method: String? get() = definedExternally; set(value) = definedExternally
    var onComplete: Function<*>
}

external interface IUtilDomRequest {
    fun request(url: String, options: `T$48`? = definedExternally /* null */): XMLHttpRequest
}

external interface IUtilDomStyle {
    fun setStyle(element: HTMLElement, styles: Any): HTMLElement
}

external interface IUtilArray {
    fun invoke(array: Array<Any>, method: String): Array<Any>
    fun min(array: Array<Any>, byProperty: String): Any
    fun max(array: Array<Any>, byProperty: String): Any
}

external interface IUtilClass {
    fun createClass(parent: Function<*>, properties: Any? = definedExternally /* null */): Any
    fun createClass(properties: Any? = definedExternally /* null */): Any
}

external interface IUtilObject {
    fun extend(destination: Any, source: Any): Any
    fun clone(`object`: Any): Any
}

external interface IUtilString {
    fun camelize(string: String): String
    fun capitalize(string: String, firstLetterOnly: Boolean): String
    fun escapeXml(string: String): String
    fun graphemeSplit(string: String): Array<String>
}

external interface `T$49` {
    var angle: Number
    var scaleX: Number
    var scaleY: Number
    var skewX: Number
    var skewY: Number
    var translateX: Number
    var translateY: Number
}

external interface `T$50` {
    var scaleX: Number
    var scaleY: Number
    var skewX: Number
    var skewY: Number
    var angle: Number
    var left: Number
    var flipX: Boolean
    var flipY: Boolean
    var top: Number
}

external interface IUtilMisc {
    fun removeFromArray(array: Array<Any>, value: Any): Array<Any>
    fun getRandomInt(min: Number, max: Number): Number
    fun degreesToRadians(degrees: Number): Number
    fun radiansToDegrees(radians: Number): Number
    fun rotatePoint(point: Point, origin: Point, radians: Number): Point
    fun rotateVector(vector: `T$4`, radians: Number): `T$4`
    fun transformPoint(p: Point, t: Array<Any>, ignoreOffset: Boolean? = definedExternally /* null */): Point
    fun invertTransform(t: Array<Any>): Array<Any>
    fun toFixed(number: Number, fractionDigits: Number): Number
    fun parseUnit(value: Number, fontSize: Number? = definedExternally /* null */): dynamic /* Number | String */
    fun parseUnit(value: String, fontSize: Number? = definedExternally /* null */): dynamic /* Number | String */
    fun falseFunction(): Boolean
    fun getKlass(type: String, namespace: String): Any
    fun resolveNamespace(namespace: String): Any
    fun loadImage(
        url: String,
        callback: (image: HTMLImageElement) -> Unit,
        context: Any? = definedExternally /* null */,
        crossOrigin: String? = definedExternally /* null */
    )

    fun enlivenObjects(
        objects: Array<Any>,
        callback: Function<*>,
        namespace: String,
        reviver: Function<*>? = definedExternally /* null */
    )

    fun groupSVGElements(
        elements: Array<Any>,
        options: Any? = definedExternally /* null */,
        path: String? = definedExternally /* null */
    ): dynamic /* Any | Group */

    fun populateWithProperties(source: Any, destination: Any, properties: Any)
    fun drawDashedLine(ctx: CanvasRenderingContext2D, x: Number, y: Number, x2: Number, y2: Number, da: Array<Any>)
    fun createCanvasElement(canvasEl: HTMLCanvasElement? = definedExternally /* null */): HTMLCanvasElement
    fun createImage(): HTMLImageElement
    fun createAccessors(klass: Any): Any
    fun clipContext(receiver: Any, ctx: CanvasRenderingContext2D)
    fun multiplyTransformMatrices(a: Array<Number>, b: Array<Number>): Array<Number>
    fun qrDecompose(a: Array<Number>): `T$49`
    fun saveObjectTransform(target: Any): `T$50`
    fun customTransformMatrix(scaleX: Number, scaleY: Number, skewX: Number): Array<Number>
    fun getFunctionBody(fn: Function<*>): String
    fun isTransparent(ctx: CanvasRenderingContext2D, x: Number, y: Number, tolerance: Number): Boolean
}

external var util: IUtil = definedExternally

external interface IUtil : IUtilAnimation, IUtilArc, IObservable<IUtil>, IUtilDomEvent, IUtilDomMisc, IUtilDomRequest,
    IUtilDomStyle, IUtilClass, IUtilMisc {
    var ease: IUtilAnimEase
    var array: IUtilArray
    var `object`: IUtilObject
    var string: IUtilString
}

external interface Resources {
    @nativeGetter
    operator fun get(key: String): HTMLCanvasElement?

    @nativeSetter
    operator fun set(key: String, value: HTMLCanvasElement)
}

external interface FilterBackend {
    var resources: Resources
    fun applyFilters(
        filters: Array<IBaseFilter>,
        sourceElement: HTMLImageElement,
        sourceWidth: Number,
        sourceHeight: Number,
        targetCanvas: HTMLCanvasElement,
        cacheKey: String? = definedExternally /* null */
    ): Any

    fun applyFilters(
        filters: Array<IBaseFilter>,
        sourceElement: HTMLCanvasElement,
        sourceWidth: Number,
        sourceHeight: Number,
        targetCanvas: HTMLCanvasElement,
        cacheKey: String? = definedExternally /* null */
    ): Any

    fun evictCachesForKey(cacheKey: String)
    fun dispose()
    fun clearWebGLCaches()
}

external var filterBackend: FilterBackend? = definedExternally

//open external class Canvas2dFilterBackend : FilterBackend
external interface GPUInfo {
    var renderer: String
    var vendor: String
}

external interface WebglFilterBackendOptions {
    var tileSize: Number
}

//open external class WebglFilterBackend(options: WebglFilterBackendOptions? = definedExternally /* null */) :
//    FilterBackend, WebglFilterBackendOptions {
//    fun setupGLContext(width: Number, height: Number)
//    fun chooseFastestCopyGLTo2DMethod(width: Number, height: Number)
//    fun createWebGLCanvas(width: Number, height: Number)
//    fun applyFiltersDebug(
//        filters: Array<IBaseFilter>,
//        sourceElement: HTMLImageElement,
//        sourceWidth: Number,
//        sourceHeight: Number,
//        targetCanvas: HTMLCanvasElement,
//        cacheKey: String? = definedExternally /* null */
//    ): Any
//
//    fun applyFiltersDebug(
//        filters: Array<IBaseFilter>,
//        sourceElement: HTMLCanvasElement,
//        sourceWidth: Number,
//        sourceHeight: Number,
//        targetCanvas: HTMLCanvasElement,
//        cacheKey: String? = definedExternally /* null */
//    ): Any
//
//    fun glErrorToString(context: Any, errorCode: Any): String
//    fun createTexture(
//        gl: WebGLRenderingContext,
//        width: Number,
//        height: Number,
//        textureImageSource: HTMLImageElement? = definedExternally /* null */
//    ): WebGLTexture
//
//    fun createTexture(
//        gl: WebGLRenderingContext,
//        width: Number,
//        height: Number,
//        textureImageSource: HTMLCanvasElement? = definedExternally /* null */
//    ): WebGLTexture
//
//    fun getCachedTexture(uniqueId: String, textureImageSource: HTMLImageElement): WebGLTexture
//    fun getCachedTexture(uniqueId: String, textureImageSource: HTMLCanvasElement): WebGLTexture
//    fun copyGLTo2D(gl: WebGLRenderingContext, pipelineState: Any)
//    fun captureGPUInfo(): GPUInfo
//    fun createTexture(gl: WebGLRenderingContext, width: Number, height: Number): WebGLTexture
//}
