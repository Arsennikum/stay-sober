package ru.keepsober

import kotlinx.html.dom.create
import kotlinx.html.js.div
import kotlinx.html.js.h5
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLImageElement
import ru.keepsober.util.Size
import ru.keepsober.util.loadCss
import ru.keepsober.wrapper.fabric.Canvas
import ru.keepsober.wrapper.fabric.Image
import kotlin.browser.document
import kotlin.browser.window


external fun setTimeout(f: () -> Unit, ms: Int)

lateinit var userImageElement: HTMLImageElement
lateinit var canvas: Canvas
lateinit var image: Image
lateinit var badge: Badge

fun main() {
    loadCss(
        "https://fonts.googleapis.com/" +
                "css?family=${Config.fontLooks.mapNotNull { if (!it.system) it.font else null }.joinToString(separator = "|")}" +
                "&display=swap&subset=cyrillic"
    )
    window.onload = {
        loadPage()
    }
}

fun loadPage() {
    canvas = buildCanvas()

    userImageElement = buildImageElement {
        canvas.clear()

        calcAndSet()

        image = Image(userImageElement, null).apply { selectable = false }
        image.scaleToWidth(canvas.getWidth(), false)

        canvas.add(image)
        image.moveTo(-1)

        badge = Badge()
        badge.firstDraw(canvas)
    }

    document.body!!.append(userImageElement)

    val inputImage = buildInputImage(userImageElement)

    val btnLoadImage = buildBtnLoadImage(inputImage)

    val btnRotateImage = buildBtnRotateImage {
        val angle = image.angle!!.toInt() + Config.rotateAngleDegrees
        image.rotate(angle)

        val width = canvas.getWidth()
        val height = canvas.getHeight()
        canvas.setWidth(height)
        canvas.setHeight(width)

        image.center()
        badge.redraw(canvas)

        canvas.renderAll()
    }

    val btnSaveImage = buildBtnSaveImage {
        canvas.getElement().toBlob({ blob ->
            val a = createBlankA()
            a.href = js("window.URL || window.webkitURL").createObjectURL(blob)
            a.download = "keep-sober.png"
            canvas.discardActiveObject()
            canvas.renderAll()
            a.click()
        })
    }

    val fontFamily = buildFontFamilySelect {
        Config.currentFontLook = Config.fontLooks[it.toInt()]

        badge.redraw(canvas)
    }

    val lookSelect = buildLookSelect {
        Config.currentLook = Config.looks[it.toInt()]

        badge.redraw(canvas)
    }

    document.body!!.prepend(document.create.div { }.also {
        it.style.width = "350pt"
        it.style.maxWidth = "100%"
        it.style.padding = "10pt"

        btnLoadImage.style.marginBottom = "10pt"
        btnLoadImage.style.width = "100%"

        it.appendChild(btnLoadImage)
        it.appendChild(buildBr())
        it.appendChild(document.create.div { }.also {

            it.appendChild(document.create.div { }.also {
                it.style.width = "210pt"
                it.style.maxWidth = "100%"
                it.style.margin = "auto"
                it.style.textAlign = "left"

                btnRotateImage.style.width = "100%"
                btnRotateImage.style.marginBottom = "10pt"

                it.appendChild(btnRotateImage)
                it.appendChild(buildBr())

                val fontLabel = document.create.h5 { +"Шрифт:  " }
                fontLabel.appendChild(fontFamily)
                it.appendChild(fontLabel)

                val lookLabel = document.create.h5 { +"Вид:  " }
                lookLabel.appendChild(lookSelect)
                it.appendChild(lookLabel)
            })

            btnSaveImage.style.width = "100%"
            it.appendChild(btnSaveImage)
        })
    })
    document.body!!.appendChild(inputImage)

}

fun calcAndSet() {
    val canvasWrapper = document.getElementById("canvas-wrapper") as? HTMLDivElement
        ?: throw Exception("Canvas-wrapper not found")

    val frameSize = Size(canvasWrapper.clientWidth, canvasWrapper.clientHeight)
    val imageSize = Size(userImageElement.width, userImageElement.height)

    val sizeForWrap = imageSize.toSizeForWrapIn(frameSize)

    canvas.setWidth(sizeForWrap.width)
    canvas.setHeight(sizeForWrap.height)
}
