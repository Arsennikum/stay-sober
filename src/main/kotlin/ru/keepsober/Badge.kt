package ru.keepsober

import ru.keepsober.wrapper.fabric.Canvas
import ru.keepsober.wrapper.fabric.Circle
import ru.keepsober.wrapper.fabric.Group
import ru.keepsober.wrapper.fabric.Object
import ru.keepsober.wrapper.fabric.Text
import kotlin.math.min

class Badge {

    fun firstDraw(canvas: Canvas) {
        draw(canvas)
        firstInit(canvas)
    }

    fun redraw(canvas: Canvas) {
        canvas.remove(group)
        draw(canvas)
    }

    private fun groupSize() {
        val scale = min(canvas.getWidth().toDouble(), canvas.getHeight().toDouble()) * 2 / 3
        group.scaleToWidth(scale)

        group.left = canvas.getWidth().toDouble() - group.width!!.toDouble() / 2
        group.top = canvas.getHeight().toDouble() - group.height!!.toDouble() / 2

        canvas.renderAll()

    }

    private fun draw(canvas: Canvas) {
        group.removeWithUpdate(ring, circle, text)

        ring.applyRingOptions()
        circle.applyCircleOptions()
        text.applyOptions()

        group.addWithUpdate(ring, circle, text)

        groupSize()
        canvas.add(group)
        group.moveTo(1)

        canvas.renderAll()
    }

    private fun firstInit(canvas: Canvas) {
        Config.fontLooks.forEach {
            group.removeWithUpdate(text)
            text.fontFamily = it.font
            group.addWithUpdate(text)

            canvas.renderAll()
        }

        group.removeWithUpdate(text)
        text.applyOptions()
        group.addWithUpdate(text)

        groupSize()
        canvas.renderAll()
    }

    private val ring = Circle().applyRingOptions()

    private val circle = Circle().applyCircleOptions()

    private val text = Text(Config.currentLook.text).applyOptions()

    private var group = Group(arrayOf(circle, ring, text)).apply {
        set("left", 100)
        set("top", 100)
        set("selectable", true)
        set("originX", "center")
        set("originY", "center")
    }
}

private fun Circle.applyRingOptions() = apply {
    set("radius", Config.radius)
    set("fill", "transparent")
    set("stroke", Config.currentLook.color)
    set("strokeWidth", Config.ringWidth)
    set("opacity", Config.opacity)
    set("originX", "center")
    set("originY", "center")
}

private fun Circle.applyCircleOptions() = apply {
    set("radius", Config.radius - Config.ringWidth)
    set("fill", Config.currentLook.color)
    set("opacity", Config.opacity)
    set("originX", "center")
    set("originY", "center")
}

private fun Text.applyOptions() = apply {
    set("fontFamily", Config.currentFontLook.font)
    set("fontSize", Config.currentFontLook.textSize)
    set("fill", Config.currentLook.textColor)
    set("originX", "center")
    set("originY", "center")
    set("textAlign", "center")
}

private fun Group.removeWithUpdate(vararg objects: Object) {
    objects.forEach {
        removeWithUpdate(it)
    }
}

private fun Group.addWithUpdate(vararg objects: Object) {
    objects.forEach {
        addWithUpdate(it)
    }
}
