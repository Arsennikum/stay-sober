package ru.keepsober

import ru.keepsober.util.Size

fun Size.toSizeForWrapIn(wrapperSize: Size): Size {
    var res = this
    if (res.width > wrapperSize.width) {
        res = scaleToWidth(wrapperSize.width)
    }
    if (res.height > wrapperSize.height) {
        res = scaleToHeight(wrapperSize.height)
    }
    return res
}

fun Size.scaleToWidth(newWidth: Number) = Size(newWidth, height * newWidth.toDouble() / width)
fun Size.scaleToHeight(newHeight: Number) = Size(width * newHeight.toDouble() / height, newHeight)
