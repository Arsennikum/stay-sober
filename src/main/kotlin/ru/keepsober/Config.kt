package ru.keepsober

object Config {
    val fontLooks = listOf(
        FontLook(font = "Arial", textSize = 17, system = true),
        FontLook("Oswald", 19),
        FontLook("Montserrat"),
        FontLook("Merriweather"),
        FontLook("Open Sans", 17),
        FontLook("Roboto Slab"))
    var currentFontLook = fontLooks[0]

    var looks = listOf(Look("white","black"), Look("gray", "black"), Look("blue","white"))
    var currentLook = looks[0]

    const val opacity = 0.7
    const val ringWidth = 5
    const val radius = 50

    const val rotateAngleDegrees = 90;
}

data class FontLook(
    val font: String,
    val textSize: Int = 16,
    val system: Boolean = false
)

data class Look(
    var color: String = "blue",
    var textColor: String = "white",
    var text: String = "оставайся\nТРЕЗВЫМ"
)
