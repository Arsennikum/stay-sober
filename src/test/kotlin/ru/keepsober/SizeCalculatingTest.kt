package ru.keepsober

import ru.keepsober.util.Size
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SizeCalculatingTest {

    @Test
    fun widthScaling() {
        assertEquals(Size(5, 10), Size(10, 20).scaleToWidth(5))
        assertEquals(Size(10, 5), Size(20, 10).scaleToWidth(10))
    }

    @Test
    fun heightScaling() {
        assertEquals(Size(5, 10), Size(10, 20).scaleToHeight(10))
        assertEquals(Size(10, 5), Size(20, 10).scaleToHeight(5))
    }

    @Test
    @JsName("sizeCalc")
    fun `size calculating`() {
        val imageSizes = listOf(Size(100.0, 200.0), Size(300.0, 10.0))
        val frameSizes = listOf(Size(10.0, 20.0), Size(300.0, 10.0))
        val resultSizes = listOf(Size(10, 20), Size(300, 10))

        for (i in resultSizes.indices) {
            assertEquals(resultSizes[i], imageSizes[i].toSizeForWrapIn(frameSizes[i]))
        }

    }

}
